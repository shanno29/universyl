import { UnisylPage } from './app.po';

describe('unisyl App', () => {
  let page: UnisylPage;

  beforeEach(() => {
    page = new UnisylPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });

});
