"use strict";
exports.__esModule = true;
var assert = require("assert");
var Util_1 = require("../../app/_core/util/Util");
describe('includes', function () {
    it('includes', function () {
        assert.equal(Util_1.Util.includes('a', 'a'), true);
        assert.equal(Util_1.Util.includes('a', 'A'), true);
        assert.equal(Util_1.Util.includes('A', 'a'), true);
        assert.equal(Util_1.Util.includes('A', 'A'), true);
        assert.equal(Util_1.Util.includes('a', 'b'), false);
        assert.equal(Util_1.Util.includes('a', 'B'), false);
        assert.equal(Util_1.Util.includes('A', 'b'), false);
        assert.equal(Util_1.Util.includes('A', 'B'), false);
    });
});
