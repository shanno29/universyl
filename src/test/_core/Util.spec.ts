import * as assert from 'assert';
import {Util} from '../../app/_core/util/Util';

describe('includes', function () {

  it('includes', function () {
    assert.equal(Util.includes('a', 'a'), true);
    assert.equal(Util.includes('a', 'A'), true);
    assert.equal(Util.includes('A', 'a'), true);
    assert.equal(Util.includes('A', 'A'), true);

    assert.equal(Util.includes('a', 'b'), false);
    assert.equal(Util.includes('a', 'B'), false);
    assert.equal(Util.includes('A', 'b'), false);
    assert.equal(Util.includes('A', 'B'), false);
  });

});
