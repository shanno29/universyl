"use strict";
exports.__esModule = true;
var assert = require("assert");
var Support_1 = require("../../../app/domain/syllabus/Support");
describe('Material', function () {
    it('constructor', function () {
        var support = new Support_1.Support();
        assert.equal(isNaN(support.id), false);
        assert.equal(support.campusResources, '');
        assert.equal(support.additionalHelp, '');
        assert.equal(support.productivityTips, '');
        assert.equal(support.studentResponsibilities, '');
        assert.equal(support.studentFeedback, '');
        assert.equal(support.other, '');
    });
});
