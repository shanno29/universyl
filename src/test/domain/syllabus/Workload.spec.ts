import * as assert from 'assert';
import {Workload} from '../../../app/domain/syllabus/Workload';

describe('Workload', function () {

  it('constructor', function () {
    const workload: Workload = new Workload();
    assert.equal(isNaN(workload.id), false);
    assert.equal(workload.exams, '');
    assert.equal(workload.quizzes, '');
    assert.equal(workload.presentations, '');
    assert.equal(workload.exercises, '');
    assert.equal(workload.labs, '');
    assert.equal(workload.projects, '');
    assert.equal(workload.papers, '');
    assert.equal(workload.others, '');
  });

});
