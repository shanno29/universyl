"use strict";
exports.__esModule = true;
var assert = require("assert");
var CampusPolicy_1 = require("../../../app/domain/syllabus/CampusPolicy");
describe('Campus Policy', function () {
    it('constructor', function () {
        var campusPolicy = new CampusPolicy_1.CampusPolicy();
        assert.equal(isNaN(campusPolicy.id), false);
        assert.equal(campusPolicy.academicConduct, '');
        assert.equal(campusPolicy.disabilityPolicy, '');
    });
});
