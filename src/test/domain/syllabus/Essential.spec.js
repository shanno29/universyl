"use strict";
exports.__esModule = true;
var assert = require("assert");
var Essential_1 = require("../../../app/domain/syllabus/Essential");
describe('Essential', function () {
    it('constructor', function () {
        var essential = new Essential_1.Essential();
        assert.equal(isNaN(essential.id), false);
        assert.equal(essential.professorName, '');
        assert.equal(essential.officeHours, '');
        assert.equal(essential.phoneNumber, '');
        assert.equal(essential.emailAddress, '');
        assert.equal(essential.welcomeStatement, '');
        assert.equal(essential.teachingPhilosophy, '');
    });
});
