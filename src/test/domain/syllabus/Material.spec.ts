import * as assert from 'assert';
import {Materials} from '../../../app/domain/syllabus/Materials';

describe('Material', function () {

  it('constructor', function () {
    const material: Materials = new Materials();
    assert.equal(isNaN(material.id), false);
    assert.equal(material.books, '');
    assert.equal(material.films, '');
    assert.equal(material.reports, '');
    assert.equal(material.readings, '');
    assert.equal(material.software, '');
    assert.equal(material.hardware, '');
    assert.equal(material.equipment, '');
    assert.equal(material.fieldTrips, '');
    assert.equal(material.serviceLearning, '');
    assert.equal(material.seminars, '');
    assert.equal(material.eventAttendance, '');
    assert.equal(material.libraryReserves, '');
  });

});
