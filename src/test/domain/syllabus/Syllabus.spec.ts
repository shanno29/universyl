import * as assert from 'assert';
import {Syllabus} from '../../../app/domain/syllabus/Syllabus';
import {isNullOrUndefined} from 'util';

describe('Syllabus', function () {

  it('constructor', function () {
    const syllabus: Syllabus = new Syllabus();
    assert.equal(isNaN(syllabus.id), false);
    assert.equal(isNullOrUndefined(syllabus.course), true);
    assert.equal(isNullOrUndefined(syllabus.essential), true);
    assert.equal(isNullOrUndefined(syllabus.workload), true);
    assert.equal(isNullOrUndefined(syllabus.materials), true);
    assert.equal(isNullOrUndefined(syllabus.campusPolicy), true);
    assert.equal(isNullOrUndefined(syllabus.unitPolicy), true);
    assert.equal(isNullOrUndefined(syllabus.instructorPolicy), true);
    assert.equal(isNullOrUndefined(syllabus.gradingPolicy), true);
    assert.equal(isNullOrUndefined(syllabus.support), true);
  });

});
