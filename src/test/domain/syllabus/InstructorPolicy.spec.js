"use strict";
exports.__esModule = true;
var assert = require("assert");
var InstructorPolicy_1 = require("../../../app/domain/syllabus/InstructorPolicy");
describe('InstructorPolicy', function () {
    it('constructor', function () {
        var instructorPolicy = new InstructorPolicy_1.InstructorPolicy();
        assert.equal(isNaN(instructorPolicy.id), false);
        assert.equal(instructorPolicy.attendance, '');
        assert.equal(instructorPolicy.lateAssignments, '');
    });
});
