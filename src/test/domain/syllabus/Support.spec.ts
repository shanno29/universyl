import * as assert from 'assert';
import {Support} from '../../../app/domain/syllabus/Support';

describe('Material', function () {

  it('constructor', function () {
    const support: Support = new Support();
    assert.equal(isNaN(support.id), false);
    assert.equal(support.campusResources, '');
    assert.equal(support.additionalHelp, '');
    assert.equal(support.productivityTips, '');
    assert.equal(support.studentResponsibilities, '');
    assert.equal(support.studentFeedback, '');
    assert.equal(support.other, '');
  });

});
