import * as assert from 'assert';
import {InstructorPolicy} from '../../../app/domain/syllabus/InstructorPolicy';

describe('InstructorPolicy', function () {

  it('constructor', function () {
    const instructorPolicy: InstructorPolicy = new InstructorPolicy();
    assert.equal(isNaN(instructorPolicy.id), false);
    assert.equal(instructorPolicy.attendance, '');
    assert.equal(instructorPolicy.lateAssignments, '');
  });

});
