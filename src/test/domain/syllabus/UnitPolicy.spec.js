"use strict";
exports.__esModule = true;
var assert = require("assert");
var UnitPolicy_1 = require("../../../app/domain/syllabus/UnitPolicy");
describe('UnitPolicy', function () {
    it('constructor', function () {
        var unitPolicy = new UnitPolicy_1.UnitPolicy();
        assert.equal(isNaN(unitPolicy.id), false);
        assert.equal(unitPolicy.departmentResources, '');
        assert.equal(unitPolicy.gradeAppeal, '');
        assert.equal(unitPolicy.grievance, '');
        assert.equal(unitPolicy.other, '');
    });
});
