import * as assert from 'assert';
import {Essential} from '../../../app/domain/syllabus/Essential';

describe('Essential', function () {

  it('constructor', function () {
    const essential: Essential = new Essential();
    assert.equal(isNaN(essential.id), false);
    assert.equal(essential.professorName, '');
    assert.equal(essential.officeHours, '');
    assert.equal(essential.phoneNumber, '');
    assert.equal(essential.emailAddress, '');
    assert.equal(essential.welcomeStatement, '');
    assert.equal(essential.teachingPhilosophy, '');
  });

});
