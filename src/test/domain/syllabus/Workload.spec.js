"use strict";
exports.__esModule = true;
var assert = require("assert");
var Workload_1 = require("../../../app/domain/syllabus/Workload");
describe('Workload', function () {
    it('constructor', function () {
        var workload = new Workload_1.Workload();
        assert.equal(isNaN(workload.id), false);
        assert.equal(workload.exams, '');
        assert.equal(workload.quizzes, '');
        assert.equal(workload.presentations, '');
        assert.equal(workload.exercises, '');
        assert.equal(workload.labs, '');
        assert.equal(workload.projects, '');
        assert.equal(workload.papers, '');
        assert.equal(workload.others, '');
    });
});
