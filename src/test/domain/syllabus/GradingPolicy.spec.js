"use strict";
exports.__esModule = true;
var assert = require("assert");
var GradingPolicy_1 = require("../../../app/domain/syllabus/GradingPolicy");
describe('GradingPolicy', function () {
    it('constructor', function () {
        var gradingPolicy = new GradingPolicy_1.GradingPolicy();
        assert.equal(isNaN(gradingPolicy.id), false);
        assert.equal(gradingPolicy.attendance, '');
        assert.equal(gradingPolicy.lateAssignments, '');
        assert.equal(gradingPolicy.makeUpOptions, '');
        assert.equal(gradingPolicy.extraCredit, '');
        assert.equal(gradingPolicy.deadlineExtensions, '');
        assert.equal(gradingPolicy.reportingIllness, '');
        assert.equal(gradingPolicy.plagiarism, '');
        assert.equal(gradingPolicy.classroomConduct, '');
        assert.equal(gradingPolicy.gradingScale, '');
        assert.equal(gradingPolicy.rubrics, '');
    });
});
