"use strict";
exports.__esModule = true;
var assert = require("assert");
var Syllabus_1 = require("../../../app/domain/syllabus/Syllabus");
var util_1 = require("util");
describe('Syllabus', function () {
    it('constructor', function () {
        var syllabus = new Syllabus_1.Syllabus();
        assert.equal(isNaN(syllabus.id), false);
        assert.equal(util_1.isNullOrUndefined(syllabus.course), true);
        assert.equal(util_1.isNullOrUndefined(syllabus.essential), true);
        assert.equal(util_1.isNullOrUndefined(syllabus.workload), true);
        assert.equal(util_1.isNullOrUndefined(syllabus.materials), true);
        assert.equal(util_1.isNullOrUndefined(syllabus.campusPolicy), true);
        assert.equal(util_1.isNullOrUndefined(syllabus.unitPolicy), true);
        assert.equal(util_1.isNullOrUndefined(syllabus.instructorPolicy), true);
        assert.equal(util_1.isNullOrUndefined(syllabus.gradingPolicy), true);
        assert.equal(util_1.isNullOrUndefined(syllabus.support), true);
    });
});
