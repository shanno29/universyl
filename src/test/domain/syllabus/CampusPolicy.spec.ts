import * as assert from 'assert';
import {CampusPolicy} from '../../../app/domain/syllabus/CampusPolicy';

describe('Campus Policy', function () {

  it('constructor', function () {
    const campusPolicy: CampusPolicy = new CampusPolicy();
    assert.equal(isNaN(campusPolicy.id), false);
    assert.equal(campusPolicy.academicConduct, '');
    assert.equal(campusPolicy.disabilityPolicy, '');
  });

});
