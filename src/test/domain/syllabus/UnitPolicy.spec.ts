import * as assert from 'assert';
import {UnitPolicy} from '../../../app/domain/syllabus/UnitPolicy';

describe('UnitPolicy', function () {

  it('constructor', function () {
    const unitPolicy: UnitPolicy = new UnitPolicy();
    assert.equal(isNaN(unitPolicy.id), false);
    assert.equal(unitPolicy.departmentResources, '');
    assert.equal(unitPolicy.gradeAppeal, '');
    assert.equal(unitPolicy.grievance, '');
    assert.equal(unitPolicy.other, '');

  });

});
