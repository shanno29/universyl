"use strict";
exports.__esModule = true;
var assert = require("assert");
var Department_1 = require("../../../app/domain/department/Department");
describe('Department', function () {
    it('constructor', function () {
        var dept = new Department_1.Department('asdf');
        assert.equal(isNaN(dept.id), false);
        assert.equal(dept.name, 'asdf');
        assert.equal(dept.certificates.length, 0);
        assert.equal(dept.minors.length, 0);
        assert.equal(dept.bachelors.length, 0);
        assert.equal(dept.masters.length, 0);
        assert.equal(dept.graduates.length, 0);
        assert.equal(dept.doctorates.length, 0);
        assert.equal(dept.prereq.length, 0);
        assert.equal(dept.links.length, 0);
        assert.equal(dept.experience.length, 0);
    });
    it('count', function () {
        var dept = new Department_1.Department('asdf');
        assert.equal(dept.count(), 0);
    });
});
