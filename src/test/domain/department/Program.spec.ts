import * as assert from 'assert';
import {Program} from '../../../app/domain/program/Program';

describe('Program', function () {

  it('constructor', function () {
    const program: Program = new Program('asdf');
    assert.equal(isNaN(program.id), false);
    assert.equal(program.name, 'asdf');
    assert.equal(program.url, '');
    assert.equal(program.courses.length, 0);
  });

});
