"use strict";
exports.__esModule = true;
var assert = require("assert");
var Program_1 = require("../../../app/domain/program/Program");
describe('Program', function () {
    it('constructor', function () {
        var program = new Program_1.Program('asdf');
        assert.equal(isNaN(program.id), false);
        assert.equal(program.name, 'asdf');
        assert.equal(program.url, '');
        assert.equal(program.courses.length, 0);
    });
});
