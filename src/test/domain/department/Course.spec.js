"use strict";
exports.__esModule = true;
var assert = require("assert");
var Course_1 = require("../../../app/domain/syllabus/Course");
describe('Course', function () {
    it('constructor', function () {
        var course = new Course_1.Course();
        assert.equal(isNaN(course.id), false);
        assert.equal(course.title, '');
        assert.equal(course.number, '');
        assert.equal(course.description, '');
        assert.equal(course.semester, '');
        assert.equal(course.year, '');
        assert.equal(course.url, '');
        assert.equal(course.creditHours, '');
        assert.equal(course.type, '');
        assert.equal(course.preRequisites, '');
        assert.equal(course.coRequisites, '');
        assert.equal(course.computerLiteracy, '');
        assert.equal(course.goals, '');
        assert.equal(course.learningOutcomes, '');
        assert.equal(course.calendar, '');
        assert.equal(course.readingLiteracy, '');
    });
});
