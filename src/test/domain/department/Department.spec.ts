import * as assert from 'assert';
import {Department} from '../../../app/domain/department/Department';

describe('Department', function () {

  it('constructor', function () {
    const dept: Department = new Department(0, 'asdf', []);
    assert.equal(isNaN(dept.id), false);
    assert.equal(dept.name, 'asdf');
    // assert.equal(dept.programs[DeptType.Certificates].length, 0);
    // assert.equal(dept.programs[DeptType.Minors].length, 0);
    // assert.equal(dept.programs[DeptType.Bachelors].length, 0);
    // assert.equal(dept.programs[DeptType.PreProfessionals].length, 0);
    // assert.equal(dept.programs[DeptType.Masters].length, 0);
    // assert.equal(dept.programs[DeptType.GraduateCertificates].length, 0);
    // assert.equal(dept.programs[DeptType.Doctorates].length, 0);
    // assert.equal(dept.programs[DeptType.Links].length, 0);
    // assert.equal(dept.programs[DeptType.Experience].length, 0);
  });

  it('count', function () {
    const dept: Department = new Department(0, 'asdf', []);
    assert.equal(dept.count(), 0);
  });

});
