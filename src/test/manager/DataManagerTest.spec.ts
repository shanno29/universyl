import * as assert from 'assert';
import {DataManager} from '../../app/manager/data/DataManager';
import {Constant} from '../../app/_core/Constant';
import {Department} from '../../app/domain/department/Department';
import {Program, ProgramType} from '../../app/domain/program/Program';

describe('DataManagerTest', function () {

  const x: object = [
      [Constant.DEPARTMENT, [
        new Department(1, 'a', [10, 11, 12]),
        new Department(2, 'b', [20, 21, 22])
      ]],
      [Constant.PROGRAM, [
        new Program(10, 'c', ProgramType.Certificates, []),
        new Program(11, 'd', ProgramType.Minors, []),
        new Program(12, 'e', ProgramType.Bachelors, []),
        new Program(20, 'f', ProgramType.Masters, []),
        new Program(21, 'g', ProgramType.Doctorates, []),
        new Program(22, 'h', ProgramType.GraduateCertificates, [])
      ]]
    ];
    const dataManager: DataManager = new DataManager(x);

  it('constructor', function () {
    assert.equal(dataManager.departments.length, 3);
    assert.equal(dataManager.programs.length, 6);

    // assert.equal(isNaN(campusPolicy.id), false);
    // assert.equal(campusPolicy.academicConduct, '');
    // assert.equal(campusPolicy.disabilityPolicy, '');
  });

});
