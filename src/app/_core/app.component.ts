import {Component, Injector} from '@angular/core';
import {Syllabus} from '../domain/syllabus/Syllabus';
import {ToolbarEvent} from './ToolbarEvent';
import {BaseComponent} from './BaseComponent';
import {Essential} from '../domain/syllabus/Essential';
import {Course} from '../domain/syllabus/Course';
import {Workload} from '../domain/syllabus/Workload';
import {GradingPolicy} from '../domain/syllabus/GradingPolicy';
import {InstructorPolicy} from '../domain/syllabus/InstructorPolicy';
import {UnitPolicy} from '../domain/syllabus/UnitPolicy';
import {CampusPolicy} from '../domain/syllabus/CampusPolicy';
import {Support} from '../domain/syllabus/Support';
import {Materials} from '../domain/syllabus/Materials';
import {MemberType} from '../domain/user/MemberType';
import {User} from '../domain/user/User';

@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.css'],
})
export class AppComponent extends BaseComponent {

  public title = 'Universyl';
  public MemberType = MemberType;

  constructor(public syllabus: Syllabus, public user: User, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.sub.add(this.busManager
      .of(ToolbarEvent)
      .subscribe(v => this.title = v.title)
    );

    this.sub.add(this.busManager
      .of(Essential)
      .subscribe(v => this.syllabus.essential = v)
    );

    this.sub.add(this.busManager
      .of(Course)
      .subscribe(v => this.syllabus.course = v)
    );

    this.sub.add(this.busManager
      .of(Workload)
      .subscribe(v => this.syllabus.workload = v)
    );

    this.sub.add(this.busManager
      .of(Materials)
      .subscribe(v => this.syllabus.materials = v)
    );

    this.sub.add(this.busManager
      .of(GradingPolicy)
      .subscribe(v => this.syllabus.gradingPolicy = v)
    );

    this.sub.add(this.busManager
      .of(InstructorPolicy)
      .subscribe(v => this.syllabus.instructorPolicy = v)
    );

    this.sub.add(this.busManager
      .of(UnitPolicy)
      .subscribe(v => this.syllabus.unitPolicy = v)
    );

    this.sub.add(this.busManager
      .of(CampusPolicy)
      .subscribe(v => this.syllabus.campusPolicy = v)
    );

    this.sub.add(this.busManager
      .of(Support)
      .subscribe(v => {
        this.syllabus.support = v;
        setTimeout(() => this.busManager.publish(this.syllabus), 500);
      })
    );

    // On User MemberType Change
    this.sub.add(this.storageManager
      .getObjectRx('user')
      .subscribe(res => this.user = this.storageManager.getObject(res.key))
    );

    // Set Default User
    // TODO user login
    this.user.name = 'John Doe';
    this.user.type = MemberType.Student;
    this.user.email = 'johnDoe@uwm.edu';
    this.user.avatar = 'https://tinyurl.com/yckfypz4';
    this.storageManager.setObject('user', this.user);

  }

}
