import {ActivatedRoute, Data, Router} from '@angular/router';
import {Injector} from '@angular/core';
import {BusManager} from '../manager/bus/BusManager';
import {ToolbarEvent} from './ToolbarEvent';
import {Subscription} from 'rxjs/Subscription';
import {StorageManager} from '../manager/storage/StorageManager';
import {Http} from '@angular/http';
import {DataManager} from '../manager/data/DataManager';

export abstract class BaseComponent {
  public abstract title: string;
  protected storageManager: StorageManager;
  // protected dataManager: DataManager;
  protected busManager: BusManager;
  protected active: ActivatedRoute;
  protected sub: Subscription;
  protected router: Router;
  protected data: Data;
  protected http: Http;

  constructor(injector: Injector) {
    this.storageManager = injector.get(StorageManager);
    // this.dataManager = injector.get(DataManager);
    this.busManager = injector.get(BusManager);
    this.active = injector.get(ActivatedRoute);
    this.router = injector.get(Router);
    this.http = injector.get(Http);
    this.sub = new Subscription();
  }

  ngOnInit() {
    this.sub.add(this.active.data.subscribe(data => {
      this.busManager.publish(new ToolbarEvent(this.title));
      this.data = data;
    }));
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
