"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var material_1 = require("@angular/material");
var angular2_expandable_list_1 = require("angular2-expandable-list");
var angular_module_1 = require("./angular.module");
var app_component_1 = require("./app.component");
var index_1 = require("../domain/index");
var index_2 = require("../manager/index");
require("hammerjs");
var feature_module_1 = require("../feature/_core/feature.module");
var AppModule = (function () {
    function AppModule(overlayContainer) {
        overlayContainer.themeClass = 'uwm-theme';
    }

    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                angular_module_1.AngularModule,
                material_1.MaterialModule,
                angular2_expandable_list_1.ExpandableListModule,
                feature_module_1.FeatureModule,
                router_1.RouterModule.forRoot([
                    {path: '', redirectTo: 'home', pathMatch: 'full'},
                    {path: 'home', loadChildren: 'app/feature/home/home.module#HomeModule'},
                    {path: 'about', loadChildren: 'app/feature/about/about.module#AboutModule'},
                    {path: 'calendar', loadChildren: 'app/feature/calendar/calendar.module#CalendarModule'},
                    {path: 'degree', loadChildren: 'app/feature/degree/degree.module#DegreeModule'},
                    {path: 'editor', loadChildren: 'app/feature/editor/_core/editor.module#EditorModule'},
                    {path: 'explore', loadChildren: 'app/feature/explore/explore.module#ExploreModule'},
                    {path: 'help', loadChildren: 'app/feature/help/help.module#HelpModule'},
                    {path: 'settings', loadChildren: 'app/feature/settings/settings.module#SettingsModule'},
                    {path: 'stats', loadChildren: 'app/feature/stats/stats.module#StatsModule'},
                ]),
            ],
            declarations: [app_component_1.AppComponent],
            providers: [
                index_2.MANAGERS,
                index_1.MODELS
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
