"use strict";
exports.__esModule = true;
var router_1 = require("@angular/router");
var BusManager_1 = require("../manager/bus/BusManager");
var ToolbarEvent_1 = require("./ToolbarEvent");
var Subscription_1 = require("rxjs/Subscription");
var StorageManager_1 = require("../manager/storage/StorageManager");
var http_1 = require("@angular/http");
var DataManager_1 = require("../manager/data/DataManager");
var BaseComponent = (function () {
    function BaseComponent(injector) {
        this.storageManager = injector.get(StorageManager_1.StorageManager);
        this.dataManager = injector.get(DataManager_1.DataManager);
        this.busManager = injector.get(BusManager_1.BusManager);
        this.active = injector.get(router_1.ActivatedRoute);
        this.toolbar = injector.get(ToolbarEvent_1.ToolbarEvent);
        this.router = injector.get(router_1.Router);
        this.http = injector.get(http_1.Http);
        this.sub = new Subscription_1.Subscription();
    }

    BaseComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub.add(this.active.data.subscribe(function (data) {
            _this.toolbar.title = _this.title;
            _this.busManager.publish(_this.toolbar);
            _this.data = data;
        }));
    };
    BaseComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    return BaseComponent;
}());
exports.BaseComponent = BaseComponent;
