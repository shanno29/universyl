"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var ToolbarEvent_1 = require("./ToolbarEvent");
var BaseComponent_1 = require("./BaseComponent");
var Essential_1 = require("../domain/syllabus/Essential");
var Course_1 = require("../domain/syllabus/Course");
var Workload_1 = require("../domain/syllabus/Workload");
var GradingPolicy_1 = require("../domain/syllabus/GradingPolicy");
var InstructorPolicy_1 = require("../domain/syllabus/InstructorPolicy");
var UnitPolicy_1 = require("../domain/syllabus/UnitPolicy");
var CampusPolicy_1 = require("../domain/syllabus/CampusPolicy");
var Support_1 = require("../domain/syllabus/Support");
var Materials_1 = require("../domain/syllabus/Materials");
var MemberType_1 = require("../domain/user/MemberType");
var AppComponent = (function (_super) {
    __extends(AppComponent, _super);
    function AppComponent(syllabus, user, injector) {
        var _this = _super.call(this, injector) || this;
        _this.syllabus = syllabus;
        _this.user = user;
        _this.title = 'Universyl';
        _this.MemberType = MemberType_1.MemberType;
        return _this;
    }

    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub.add(this.busManager
            .of(ToolbarEvent_1.ToolbarEvent)
            .subscribe(function (v) {
                return _this.title = v.title;
            }));
        this.sub.add(this.busManager
            .of(Essential_1.Essential)
            .subscribe(function (v) {
                return _this.syllabus.essential = v;
            }));
        this.sub.add(this.busManager
            .of(Course_1.Course)
            .subscribe(function (v) {
                return _this.syllabus.course = v;
            }));
        this.sub.add(this.busManager
            .of(Workload_1.Workload)
            .subscribe(function (v) {
                return _this.syllabus.workload = v;
            }));
        this.sub.add(this.busManager
            .of(Materials_1.Materials)
            .subscribe(function (v) {
                return _this.syllabus.materials = v;
            }));
        this.sub.add(this.busManager
            .of(GradingPolicy_1.GradingPolicy)
            .subscribe(function (v) {
                return _this.syllabus.gradingPolicy = v;
            }));
        this.sub.add(this.busManager
            .of(InstructorPolicy_1.InstructorPolicy)
            .subscribe(function (v) {
                return _this.syllabus.instructorPolicy = v;
            }));
        this.sub.add(this.busManager
            .of(UnitPolicy_1.UnitPolicy)
            .subscribe(function (v) {
                return _this.syllabus.unitPolicy = v;
            }));
        this.sub.add(this.busManager
            .of(CampusPolicy_1.CampusPolicy)
            .subscribe(function (v) {
                return _this.syllabus.campusPolicy = v;
            }));
        this.sub.add(this.busManager
            .of(Support_1.Support)
            .subscribe(function (v) {
                _this.syllabus.support = v;
                setTimeout(function () {
                    return _this.busManager.publish(_this.syllabus);
                }, 500);
            }));
        // On User MemberType Change
        this.sub.add(this.storageManager
            .getObjectRx('user')
            .subscribe(function (res) {
                return _this.user = _this.storageManager.getObject(res.key);
            }));
        // Set Default User
        // TODO user login
        this.user.name = 'John Doe';
        this.user.type = MemberType_1.MemberType.Student;
        this.user.email = 'johnDoe@uwm.edu';
        this.user.avatar = 'https://tinyurl.com/yckfypz4';
        this.storageManager.setObject('user', this.user);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}(BaseComponent_1.BaseComponent));
exports.AppComponent = AppComponent;
