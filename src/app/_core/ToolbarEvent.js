"use strict";
exports.__esModule = true;
var ToolbarEvent = (function () {
    function ToolbarEvent(title) {
        if (title === void 0) {
            title = 'Unisyl';
        }
        this.title = title;
    }

    return ToolbarEvent;
}());
exports.ToolbarEvent = ToolbarEvent;
