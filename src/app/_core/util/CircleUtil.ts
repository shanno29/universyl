export class CircleUtil {

  public static toRadians = (theta) => Math.PI / 180.0 * -theta;

  public static randColor = () => '#' + (Math.random() * 0xFFFFFF << 0).toString(16);

  public static getAngle = (circle, pos) => 360.0 / circle.sectors.length * pos;

  public static getRX = (centerX, radius, theta) => centerX + radius * Math.cos(CircleUtil.toRadians(theta));

  public static getRY = (centerY, radius, theta) => centerY + radius * Math.sin(CircleUtil.toRadians(theta));

}
