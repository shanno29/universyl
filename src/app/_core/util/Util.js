"use strict";
exports.__esModule = true;
var Util = (function () {
    function Util() {
    }

    Util.getFields = function (o) {
        var items = [];
        var keys = Object.keys(o);
        for (var i = 0; i < keys.length; i++) {
            items.push({
                id: keys[i],
                title: this.prettyTitle(keys[i]),
                link: this.prettyUrl(o) + keys[i],
                text: ''
            });
        }
        return items;
    };
    Util.prettyTitle = function (s) {
        var pretty = s.replace(/([A-Z])/g, ' $1').trim();
        return pretty.charAt(0).toUpperCase() + pretty.slice(1);
    };
    Util.prettyUrl = function (o) {
        return o.constructor.name.charAt(0).toLowerCase() + o.constructor.name.slice(1) + '.';
    };
    Util.includes = function (a, b) {
        return a.toLowerCase().includes(b.toLowerCase());
    };
    return Util;
}());
exports.Util = Util;
