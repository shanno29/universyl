export class Util {

  public static getFields(o: any) {
    const items = [];
    const keys = Object.keys(o);
    for (let i = 0; i < keys.length; i++) {

      items.push({
        id   : keys[i],
        title: this.prettyTitle(keys[i]),
        link : this.prettyUrl(o) + keys[i],
        text : ''
      });

    }
    return items;
  }

  public static prettyTitle(s: string) {
    const pretty = s.replace(/([A-Z])/g, ' $1').trim();
    return pretty.charAt(0).toUpperCase() + pretty.slice(1);
  }

  public static prettyUrl(o: any) {
    return o.constructor.name.charAt(0).toLowerCase() + o.constructor.name.slice(1) + '.';
  }

  public static includes(a: String, b: String): boolean {
    return a.toLowerCase().includes(b.toLowerCase());
  }

}
