"use strict";
exports.__esModule = true;
var CircleUtil = (function () {
    function CircleUtil() {
    }

    CircleUtil.toRadians = function (theta) {
        return Math.PI / 180.0 * -theta;
    };
    CircleUtil.randColor = function () {
        return '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
    };
    CircleUtil.getAngle = function (circle, pos) {
        return 360.0 / circle.sectors.length * pos;
    };
    CircleUtil.getRX = function (centerX, radius, theta) {
        return centerX + radius * Math.cos(CircleUtil.toRadians(theta));
    };
    CircleUtil.getRY = function (centerY, radius, theta) {
        return centerY + radius * Math.sin(CircleUtil.toRadians(theta));
    };
    return CircleUtil;
}());
exports.CircleUtil = CircleUtil;
