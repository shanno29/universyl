export class Constant {
  public static readonly INSTITUTE = 'institutes';
  public static readonly DEPARTMENT = 'departments';
  public static readonly PROGRAM = 'programs';
  public static readonly SYLLABUS = 'syllabi';
  public static readonly POLICY = 'policies';
  public static readonly API = 'assets/sandbox/sample.json';
  public static readonly DATA = 'data';

  public static readonly CERTIFICATE = 'certificates';
  public static readonly MINOR = 'minors';
  public static readonly BACHELOR = 'bachelors';
  public static readonly PREPROFESSIONAL = 'preprofessionals';
  public static readonly MASTER = 'masters';
  public static readonly GRADUATECERTIFICATE = 'graduatecertificates';
  public static readonly DOCTORATE = 'doctorates';
}
