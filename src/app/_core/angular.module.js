"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var material_1 = require("@angular/material");
var http_1 = require("@angular/http");
var http_2 = require("@angular/common/http");
var animations_1 = require("@angular/platform-browser/animations");
var platform_browser_1 = require("@angular/platform-browser");
var angular_persistence_1 = require("angular-persistence");
var flex_layout_1 = require("@angular/flex-layout");
var AngularModule = (function () {
    function AngularModule() {
    }

    AngularModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                http_2.HttpClientModule,
                http_1.HttpModule,
                material_1.MaterialModule,
                flex_layout_1.FlexLayoutModule,
                angular_persistence_1.PersistenceModule,
            ],
            declarations: [],
            exports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                http_2.HttpClientModule,
                http_1.HttpModule,
                material_1.MaterialModule,
                flex_layout_1.FlexLayoutModule,
                angular_persistence_1.PersistenceModule,
            ]
        })
    ], AngularModule);
    return AngularModule;
}());
exports.AngularModule = AngularModule;
