import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {PersistenceModule} from 'angular-persistence';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule,
    FlexLayoutModule,
    PersistenceModule,
  ],
  declarations: [],
  exports     : [
    CommonModule,
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule,
    FlexLayoutModule,
    PersistenceModule,
  ]
})
export class AngularModule {
}
