import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MaterialModule, OverlayContainer} from '@angular/material';
import {ExpandableListModule} from 'angular2-expandable-list';
import {AngularModule} from './angular.module';
import {AppComponent} from './app.component';

import {MODELS} from '../domain/index';
import {MANAGERS} from '../manager/index';

import 'hammerjs';
import {FeatureModule} from '../feature/_core/feature.module';

@NgModule({
  imports     : [
    AngularModule,
    MaterialModule,
    ExpandableListModule,
    FeatureModule,
    RouterModule.forRoot([
      {path: '', redirectTo: 'stats', pathMatch: 'full'},
      {path: 'home', loadChildren: 'app/feature/home/home.module#HomeModule'},
      {path: 'about', loadChildren: 'app/feature/about/about.module#AboutModule'},
      {path: 'calendar', loadChildren: 'app/feature/calendar/calendar.module#CalendarModule'},
      {path: 'degree', loadChildren: 'app/feature/degree/degree.module#DegreeModule'},
      {path: 'editor', loadChildren: 'app/feature/editor/_core/editor.module#EditorModule'},
      {path: 'explore', loadChildren: 'app/feature/explore/explore.module#ExploreModule'},
      {path: 'help', loadChildren: 'app/feature/help/help.module#HelpModule'},
      {path: 'settings', loadChildren: 'app/feature/settings/settings.module#SettingsModule'},
      {path: 'stats', loadChildren: 'app/feature/stats/stats.module#StatsModule'},
    ]),
  ],
  declarations: [AppComponent],
  providers   : [
    MANAGERS,
    MODELS
  ],
  bootstrap   : [AppComponent],
})

export class AppModule {
  constructor(overlayContainer: OverlayContainer) {
    overlayContainer.themeClass = 'uwm-theme';
  }
}
