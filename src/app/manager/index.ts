import {BusManager} from './bus/BusManager';
import {StorageManager} from './storage/StorageManager';
import {DataManager} from './data/DataManager';

export let MANAGERS = [
  BusManager,
  StorageManager,
  // DataManager,
];
