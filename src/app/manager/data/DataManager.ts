import {Department} from '../../domain/department/Department';
import {Program} from '../../domain/program/Program';
import {Constant} from '../../_core/Constant';
import {Syllabus} from '../../domain/syllabus/Syllabus';

export class DataManager {

  public departments: Department[] = [];
  public programs: Program[] = [];
  public syllabi: Syllabus[] = [];

  constructor(data: object) {
    this.departments = data[Constant.DEPARTMENT].map(v => Department.fromJson(v));
    this.programs = data[Constant.PROGRAM].map(v => Program.fromJson(v));
  }

  // getAllInstitutes(): Observable<Array<Institute>> {
  //    return this.http.get(Constant.API)
  //        .map(v => v.json()[Constant.DATA])
  //        .flatMap(v => v[Constant.INSTITUTE])
  //        .map(v => Institute.fromJson(v))
  //        .toArray();
  // }

  getAllDepartments(): Department[] {
    return this.departments;
  }

   getDepartmentsByIds(ids: Array<number>): Program[] {
    return this.programs
      .filter(v => ids.includes(v.id));
  }

  getAllPrograms(): Program[] {
    return this.programs;
  }

  getProgramsByIds(ids: number[]): Program[] {
    return this.programs
      .filter(v => ids.includes(v.id));
  }

  // getProgramByID(id: number): Program {
  //  return this.programs
  //    .filter(v => ids.some(x => x === v.id))
  //    .
  //
  //
  // }

  // getAllSyllabi(): Observable<Array<Syllabus>> {
  //    return this.http.get(Constant.API)
  //        .map(v => v.json()[Constant.DATA])
  //        .flatMap(v => v[Constant.SYLLABUS])
  //        .map(v => Syllabus.fromJson(v))
  //        .toArray();
  // }
  //
  // getAllPolicies(): Observable<Array<Policy>> {
  //    return this.http.get(Constant.API)
  //        .map(v => v.json()[Constant.DATA])
  //        .flatMap(v => v[Constant.POLICY])
  //        .map(v => Policy.fromJson(v))
  //        .toArray();
  // }
}
