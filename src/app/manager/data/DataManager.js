"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var DataManager = (function () {
    // private programDataData: Observable<Response>;
    // private syllabiData: Observable<Response>;
    function DataManager(http) {
        this.http = http;
        // this.programDataData = this.getJson('assets/courses.json');
        // this.syllabiData = this.getJson('assets/syllabi.json');
    }

    /*
     * DEPARTMENTS
     */
    DataManager.prototype.getAllDepartments = function () {
        return this.http
            .get('assets/department/courses.json')
            .map(function (res) {
                return res.json()['departments'];
            });
    };
    /*
     * PROGRAMS
     */
    DataManager.prototype.getAllPrograms = function () {
        return this.http
            .get('assets/program/programs.json')
            .map(function (res) {
                return res.json()['programs'];
            });
    };
    /*
     * COURSE
     */
    DataManager.prototype.getAllCourses = function () {
        return this.http
            .get('assets/course/courses.json')
            .map(function (res) {
                return res.json()['programs'];
            });
    };
    /*
     * SYLLABI
     */
    DataManager.prototype.getAllSyllabi = function () {
        return this.http
            .get('assets/syllabus/syllabi.json')
            .map(function (res) {
                return res.json()['syllabi'];
            });
    };
    DataManager = __decorate([
        core_1.Injectable()
    ], DataManager);
    return DataManager;
}());
exports.DataManager = DataManager;
