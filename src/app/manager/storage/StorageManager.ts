import {Injectable} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';

@Injectable()
export class StorageManager {
  private cache: PersistenceService;
  private type: StorageType;

  constructor(cache: PersistenceService) {
    this.type = StorageType.LOCAL;
    this.cache = cache;
  }

  public getObject(key: string): any {
    return this.cache.get(key, this.type);
  }

  public getObjectRx(key: string): any {
    return this.cache.changes({key: key, type: this.type});
  }

  public setObject(key: string, object: any): void {
    this.cache.set(key, object, {type: this.type});
  }

}
