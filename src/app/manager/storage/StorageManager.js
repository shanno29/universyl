"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var angular_persistence_1 = require("angular-persistence");
var StorageManager = (function () {
    function StorageManager(cache) {
        this.type = angular_persistence_1.StorageType.LOCAL;
        this.cache = cache;
    }

    StorageManager.prototype.getObject = function (key) {
        return this.cache.get(key, this.type);
    };
    StorageManager.prototype.getObjectRx = function (key) {
        return this.cache.changes({key: key, type: this.type});
    };
    StorageManager.prototype.setObject = function (key, object) {
        this.cache.set(key, object, {type: this.type});
    };
    StorageManager = __decorate([
        core_1.Injectable()
    ], StorageManager);
    return StorageManager;
}());
exports.StorageManager = StorageManager;
