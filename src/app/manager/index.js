"use strict";
exports.__esModule = true;
var BusManager_1 = require("./bus/BusManager");
var StorageManager_1 = require("./storage/StorageManager");
var DataManager_1 = require("./data/DataManager");
exports.MANAGERS = [
    BusManager_1.BusManager,
    StorageManager_1.StorageManager,
    DataManager_1.DataManager,
];
