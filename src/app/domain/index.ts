import {CampusPolicy} from './syllabus/CampusPolicy';
import {Course} from './syllabus/Course';
import {Essential} from './syllabus/Essential';
import {GradingPolicy} from './syllabus/GradingPolicy';
import {InstructorPolicy} from './syllabus/InstructorPolicy';
import {Materials} from './syllabus/Materials';
import {Support} from './syllabus/Support';
import {Syllabus} from './syllabus/Syllabus';
import {UnitPolicy} from './syllabus/UnitPolicy';
import {Workload} from './syllabus/Workload';
import {Deadline} from './calendar/Deadline';
import {User} from './user/User';

export let MODELS = [
  CampusPolicy,
  Course,
  Essential,
  GradingPolicy,
  InstructorPolicy,
  Materials,
  Support,
  Syllabus,
  UnitPolicy,
  Workload,
  Deadline,
  User,
];

