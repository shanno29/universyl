"use strict";
exports.__esModule = true;
var CampusPolicy_1 = require("./syllabus/CampusPolicy");
var Course_1 = require("./syllabus/Course");
var Essential_1 = require("./syllabus/Essential");
var GradingPolicy_1 = require("./syllabus/GradingPolicy");
var InstructorPolicy_1 = require("./syllabus/InstructorPolicy");
var Materials_1 = require("./syllabus/Materials");
var Support_1 = require("./syllabus/Support");
var Syllabus_1 = require("./syllabus/Syllabus");
var UnitPolicy_1 = require("./syllabus/UnitPolicy");
var Workload_1 = require("./syllabus/Workload");
var ToolbarEvent_1 = require("../_core/ToolbarEvent");
var Deadline_1 = require("./calendar/Deadline");
var User_1 = require("./user/User");
exports.MODELS = [
    CampusPolicy_1.CampusPolicy,
    Course_1.Course,
    Essential_1.Essential,
    GradingPolicy_1.GradingPolicy,
    InstructorPolicy_1.InstructorPolicy,
    Materials_1.Materials,
    Support_1.Support,
    Syllabus_1.Syllabus,
    UnitPolicy_1.UnitPolicy,
    Workload_1.Workload,
    ToolbarEvent_1.ToolbarEvent,
    Deadline_1.Deadline,
    User_1.User,
];
