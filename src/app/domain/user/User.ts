import {Injectable} from '@angular/core';
import {Moment} from 'moment-mini';
import {MemberType} from './MemberType';

@Injectable()
export class User {

  id: number;
  type: MemberType;
  name: string;
  email: string;
  avatar: string;

  constructor() {
    this.id = Date.now();
  }

}
