"use strict";
exports.__esModule = true;
var MemberType;
(function (MemberType) {
    MemberType[MemberType["Student"] = 0] = "Student";
    MemberType[MemberType["Faculty"] = 1] = "Faculty";
    MemberType[MemberType["Admin"] = 2] = "Admin";
})(MemberType = exports.MemberType || (exports.MemberType = {}));
