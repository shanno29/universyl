import {Util} from '../../_core/util/Util';
import {Program} from '../program/Program';
export interface DepartmentType {
  id: number;
  name: string;
  programs: any[];
}

export class Department implements DepartmentType {

  id: number;
  name: string;
  programs: any[];

  static fromJson(data: Object) {
    const id = data['id'];
    const name = data['name'];
    const programs = data['programs'];
    return new Department(id, name, programs);
  }

  constructor(id: number, name: string, programs: any[]) {
    this.id = id;
    this.name = name;
    this.programs = programs;
  }

  public count(): number {
    return this.programs.length;
  }

   public filterDept(text: String) {
      const idMatch: boolean = this.id === Number(text);
      const nameMatch: boolean = Util.includes(this.name, text);
      const programMatch: boolean = (this.programs as Program[]).some(p => p.filterProg(text));
      return idMatch || nameMatch || programMatch;
   }

}
