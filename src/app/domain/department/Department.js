"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var Util_1 = require("../../_core/util/Util");
var Department = (function () {
    function Department(json) {
        this.id = json['id'];
        this.name = json['name'];
        this.programs = new Map();
        this.programs['certificates'] = json['certificates'];
        this.programs['minors'] = json['minors'];
        this.programs['bachelors'] = json['bachelors'];
        this.programs['masters'] = json['masters'];
        this.programs['graduates'] = json['graduates'];
        this.programs['doctorates'] = json['doctorates'];
        this.programs['pres'] = json['pres'];
        this.programs['links'] = json['links'];
        this.programs['experience'] = json['experience'];
    }

    Department_1 = Department;
    Department.prototype.filterDept = function (text) {
        var filtered = new Map();
        filtered['id'] = this.id;
        filtered['name'] = this.name;
        filtered['certificates'] = this.programs['certificates'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        filtered['minors'] = this.programs['minors'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        filtered['bachelors'] = this.programs['bachelors'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        filtered['masters'] = this.programs['masters'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        filtered['graduates'] = this.programs['graduates'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        filtered['doctorates'] = this.programs['doctorates'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        filtered['pres'] = this.programs['pres'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        filtered['links'] = this.programs['links'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        filtered['experience'] = this.programs['experience'].filter(function (item) {
            return Util_1.Util.includes(item, text);
        });
        return new Department_1(filtered);
    };
    Department.prototype.count = function () {
        var res = 0;
        res += this.programs['certificates'].length;
        res += this.programs['minors'].length;
        res += this.programs['bachelors'].length;
        res += this.programs['masters'].length;
        res += this.programs['graduates'].length;
        res += this.programs['doctorates'].length;
        res += this.programs['pres'].length;
        res += this.programs['links'].length;
        res += this.programs['experience'].length;
        return res;
    };
    Department = Department_1 = __decorate([
        core_1.Injectable()
    ], Department);
    return Department;
    var Department_1;
}());
exports.Department = Department;
