import {Util} from '../../_core/util/Util';

export class Program {

  id: number;
  name: string;
  type: string;
  syllabi: number[];

  static fromJson(data: Object) {
    const id = data['id'];
    const name = data['name'];
    const type = data['type'];
    const syllabi = data['syllabi'];
    return new Program(id, name, type, syllabi);
  }

  constructor(id: number, name: string, type: string, courses: number[]) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.syllabi = courses;
  }

  public count(): number {
    return this.syllabi.length;
  }

  public filterProg(text: String) {
      return this.id === Number(text) || Util.includes(this.name, text);
  }

}
