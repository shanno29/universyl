import {Injectable} from '@angular/core';

@Injectable()
export class CampusPolicy {

  id: number;
  academicConduct: String;
  disabilityPolicy: String;

  constructor() {
    this.id = Date.now();
    this.academicConduct = '';
    this.disabilityPolicy = '';
  }

}
