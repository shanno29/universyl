import {Injectable} from '@angular/core';

@Injectable()
export class Essential {

  id: number;
  professorName: String;
  officeHours: String;
  phoneNumber: String;
  emailAddress: String;
  welcomeStatement: String;
  teachingPhilosophy: String;

  constructor() {
    this.id = Date.now();
    this.professorName = '';
    this.officeHours = '';
    this.phoneNumber = '';
    this.emailAddress = '';
    this.welcomeStatement = '';
    this.teachingPhilosophy = '';
  }

}
