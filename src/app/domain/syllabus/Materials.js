"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var Materials = (function () {
    function Materials() {
        this.id = Date.now();
        this.books = '';
        this.films = '';
        this.reports = '';
        this.readings = '';
        this.software = '';
        this.hardware = '';
        this.equipment = '';
        this.fieldTrips = '';
        this.serviceLearning = '';
        this.seminars = '';
        this.eventAttendance = '';
        this.libraryReserves = '';
    }

    Materials = __decorate([
        core_1.Injectable()
    ], Materials);
    return Materials;
}());
exports.Materials = Materials;
