import {Injectable} from '@angular/core';

@Injectable()
export class Materials {

  id: number;
  books: String;
  films: String;
  reports: String;
  readings: String;
  software: String;
  hardware: String;
  equipment: String;
  fieldTrips: String;
  serviceLearning: String;
  seminars: String;
  eventAttendance: String;
  libraryReserves: String;

  constructor() {
    this.id = Date.now();
    this.books = '';
    this.films = '';
    this.reports = '';
    this.readings = '';
    this.software = '';
    this.hardware = '';
    this.equipment = '';
    this.fieldTrips = '';
    this.serviceLearning = '';
    this.seminars = '';
    this.eventAttendance = '';
    this.libraryReserves = '';
  }

}
