import {Injectable} from '@angular/core';

@Injectable()
export class InstructorPolicy {

  id: number;
  lateAssignments: String;
  attendance: String;

  constructor() {
    this.id = Date.now();
    this.lateAssignments = '';
    this.attendance = '';
  }

}
