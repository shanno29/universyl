import {Injectable} from '@angular/core';

@Injectable()
export class Course {

  id: number;
  title: String;
  number: String;
  description: String;
  semester: String;
  year: String;
  url: String;
  creditHours: String;
  type: String;
  preRequisites: String;
  coRequisites: String;
  computerLiteracy: String;
  goals: String;
  learningOutcomes: String;
  calendar: String;
  readingLiteracy: String;

  constructor() {
    this.id = Date.now();
    this.title = '';
    this.number = '';
    this.description = '';
    this.semester = '';
    this.year = '';
    this.url = '';
    this.creditHours = '';
    this.type = '';
    this.preRequisites = '';
    this.coRequisites = '';
    this.computerLiteracy = '';
    this.goals = '';
    this.learningOutcomes = '';
    this.calendar = '';
    this.readingLiteracy = '';
  }

}

