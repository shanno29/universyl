import {Injectable} from '@angular/core';

@Injectable()
export class UnitPolicy {

  id: number;
  departmentResources: String;
  gradeAppeal: String;
  grievance: String;
  other: String;

  constructor() {
    this.id = Date.now();
    this.departmentResources = '';
    this.gradeAppeal = '';
    this.grievance = '';
    this.other = '';
  }

}
