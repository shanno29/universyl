import {Workload} from './Workload';
import {Essential} from './Essential';
import {Support} from './Support';
import {Course} from './Course';
import {Materials} from './Materials';
import {UnitPolicy} from './UnitPolicy';
import {CampusPolicy} from './CampusPolicy';
import {InstructorPolicy} from './InstructorPolicy';
import {GradingPolicy} from './GradingPolicy';
import {Injectable} from '@angular/core';

@Injectable()
export class Syllabus {

  id: number;
  course?: Course;
  essential?: Essential;
  workload?: Workload;
  materials?: Materials;
  campusPolicy?: CampusPolicy;
  unitPolicy?: UnitPolicy;
  instructorPolicy?: InstructorPolicy;
  gradingPolicy?: GradingPolicy;
  support?: Support;

  constructor() {
    this.id = Date.now();
  }

}
