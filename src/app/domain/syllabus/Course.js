"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var Course = (function () {
    function Course() {
        this.id = Date.now();
        this.title = '';
        this.number = '';
        this.description = '';
        this.semester = '';
        this.year = '';
        this.url = '';
        this.creditHours = '';
        this.type = '';
        this.preRequisites = '';
        this.coRequisites = '';
        this.computerLiteracy = '';
        this.goals = '';
        this.learningOutcomes = '';
        this.calendar = '';
        this.readingLiteracy = '';
    }

    Course = __decorate([
        core_1.Injectable()
    ], Course);
    return Course;
}());
exports.Course = Course;
