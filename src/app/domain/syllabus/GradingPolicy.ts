import {Injectable} from '@angular/core';

@Injectable()
export class GradingPolicy {

  id: number;
  attendance: String;
  lateAssignments: String;
  makeUpOptions: String;
  extraCredit: String;
  deadlineExtensions: String;
  reportingIllness: String;
  plagiarism: String;
  classroomConduct: String;
  gradingScale: String;
  rubrics: String;

  constructor() {
    this.id = Date.now();
    this.attendance = '';
    this.lateAssignments = '';
    this.makeUpOptions = '';
    this.extraCredit = '';
    this.deadlineExtensions = '';
    this.reportingIllness = '';
    this.plagiarism = '';
    this.classroomConduct = '';
    this.gradingScale = '';
    this.rubrics = '';
  }

}
