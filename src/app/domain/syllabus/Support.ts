import {Injectable} from '@angular/core';

@Injectable()
export class Support {

  id: number;
  campusResources: String;
  additionalHelp: String;
  productivityTips: String;
  studentResponsibilities: String;
  studentFeedback: String;
  other: String;

  constructor() {
    this.id = Date.now();
    this.campusResources = '';
    this.additionalHelp = '';
    this.productivityTips = '';
    this.studentResponsibilities = '';
    this.studentFeedback = '';
    this.other = '';
  }
}

