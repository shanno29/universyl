import {Injectable} from '@angular/core';

@Injectable()
export class Workload {

  id: number;
  exams: String;
  quizzes: String;
  presentations: String;
  exercises: String;
  labs: String;
  projects: String;
  papers: String;
  others: String;

  constructor() {
    this.id = Date.now();
    this.exams = '';
    this.quizzes = '';
    this.presentations = '';
    this.exercises = '';
    this.labs = '';
    this.projects = '';
    this.papers = '';
    this.others = '';
  }

}
