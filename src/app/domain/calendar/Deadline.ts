import {Injectable} from '@angular/core';
import * as moment from 'moment-mini';
import {Moment} from 'moment-mini';

@Injectable()
export class Deadline {

  goals: [{}];

  getCurrent(): Moment {
    return moment().startOf('month');
  }

  getStart(): Date {
    return this.getCurrent().subtract(1, 'days').toDate();
  }

  getEnd(): Date {
    return this.getCurrent().add(1, 'days').toDate();
  }

}
