import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../_core/BaseComponent';

@Component({
  selector   : 'app-help',
  templateUrl: './help.component.html',
  styleUrls  : ['./help.component.css']
})
export class HelpComponent extends BaseComponent {
  public title = 'Help';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

  }

}
