import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {HelpComponent} from './help.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{path: 'help', component: HelpComponent}])
  ],
  declarations: [HelpComponent],
  exports     : [HelpComponent]
})
export class HelpModule {
}
