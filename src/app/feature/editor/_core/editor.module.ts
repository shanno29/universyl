import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {ManualModule} from '../feature/manual/manual.module';
import {ReviewModule} from '../feature/review/review.module';
import {SectionModule} from '../feature/section/_core/section.module';
import {WizardModule} from '../feature/wizard/wizard.module';
import {EditorComponent} from './editor.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{path: 'editor', component: EditorComponent}]),
    ManualModule,
    ReviewModule,
    SectionModule,
    WizardModule,
  ],
  declarations: [EditorComponent],
  exports     : [
    EditorComponent,
    ManualModule,
    ReviewModule,
    SectionModule,
    WizardModule,
  ]
})
export class EditorModule {
}


// {
//  path: 'editor/manual',
//  component: ManualComponent,
// },
// {
//  path: 'editor/wizard',
//  component: WizardComponent,
// },
// {
//  path: 'editor/wizard/essential',
//  component: EssentialComponent,
//  data: {
//    pos: 1,
//    total: 10,
//    prev: '/editor/',
//    next: '/editor/wizard/course'
//  }
// },
// {
//  path: 'editor/wizard/course',
//  component: CourseComponent,
//  data: {
//    pos: 2,
//    total: 10,
//    prev: '/editor/wizard/essential',
//    next: '/editor/wizard/workload'
//  }
// },
// {
//  path: 'editor/wizard/workload',
//  component: WorkloadComponent,
//  data: {
//    pos: 3,
//    total: 10,
//    prev: '/editor/wizard/course',
//    next: '/editor/wizard/materials'
//  }
// },
// {
//  path: 'editor/wizard/materials',
//  component: MaterialsComponent,
//  data: {
//    pos: 4,
//    total: 10,
//    prev: '/editor/wizard/workload',
//    next: '/editor/wizard/grading_policy'
//  }
// },
// {
//  path: 'editor/wizard/grading_policy',
//  component: GradingPolicyComponent,
//  data: {
//    pos: 5,
//    total: 10,
//    prev: '/editor/wizard/workload',
//    next: '/editor/wizard/instructor_policy'
//  }
// },
// {
//  path: 'editor/wizard/instructor_policy',
//  component: InstructorPolicyComponent,
//  data: {
//    pos: 6,
//    total: 10,
//    prev: '/editor/wizard/grading_policy',
//    next: '/editor/wizard/unit_policy'
//  }
// },
// {
//  path: 'editor/wizard/unit_policy',
//  component: UnitPolicyComponent,
//  data: {
//    pos: 7,
//    total: 10,
//    prev: '/editor/wizard/instructor_policy',
//    next: '/editor/wizard/campus_policy'
//  }
// },
// {
//  path: 'editor/wizard/campus_policy',
//  component: CampusPolicyComponent,
//  data: {
//    pos: 8,
//    total: 10,
//    prev: '/editor/wizard/unit_policy',
//    next: '/editor/wizard/support'
//  }
// },
// {
//  path: 'editor/wizard/support',
//  component: SupportComponent,
//  data: {
//    pos: 9,
//    total: 10,
//    prev: '/editor/wizard/campus_policy',
//    next: '/editor/wizard/review'
//  }
//
// },
// {
//  path: 'editor/wizard/review',
//  component: ReviewComponent,
//  data: {
//    pos: 10,
//    total: 10,
//    prev: '/editor/wizard/support',
//    next: '/editor/wizard/complete'
//  }
// },
