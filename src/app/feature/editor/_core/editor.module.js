"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var material_1 = require("@angular/material");
var manual_module_1 = require("../feature/manual/manual.module");
var review_module_1 = require("../feature/review/review.module");
var section_module_1 = require("../feature/section/_core/section.module");
var wizard_module_1 = require("../feature/wizard/wizard.module");
var editor_component_1 = require("./editor.component");
var router_1 = require("@angular/router");
var EditorModule = (function () {
    function EditorModule() {
    }

    EditorModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                router_1.RouterModule.forChild([{path: 'editor', component: editor_component_1.EditorComponent}]),
                manual_module_1.ManualModule,
                review_module_1.ReviewModule,
                section_module_1.SectionModule,
                wizard_module_1.WizardModule,
            ],
            declarations: [editor_component_1.EditorComponent],
            exports: [
                editor_component_1.EditorComponent,
                manual_module_1.ManualModule,
                review_module_1.ReviewModule,
                section_module_1.SectionModule,
                wizard_module_1.WizardModule,
            ]
        })
    ], EditorModule);
    return EditorModule;
}());
exports.EditorModule = EditorModule;
// {
//  path: 'editor/manual',
//  component: ManualComponent,
// },
// {
//  path: 'editor/wizard',
//  component: WizardComponent,
// },
// {
//  path: 'editor/wizard/essential',
//  component: EssentialComponent,
//  data: {
//    pos: 1,
//    total: 10,
//    prev: '/editor/',
//    next: '/editor/wizard/course'
//  }
// },
// {
//  path: 'editor/wizard/course',
//  component: CourseComponent,
//  data: {
//    pos: 2,
//    total: 10,
//    prev: '/editor/wizard/essential',
//    next: '/editor/wizard/workload'
//  }
// },
// {
//  path: 'editor/wizard/workload',
//  component: WorkloadComponent,
//  data: {
//    pos: 3,
//    total: 10,
//    prev: '/editor/wizard/course',
//    next: '/editor/wizard/materials'
//  }
// },
// {
//  path: 'editor/wizard/materials',
//  component: MaterialsComponent,
//  data: {
//    pos: 4,
//    total: 10,
//    prev: '/editor/wizard/workload',
//    next: '/editor/wizard/grading_policy'
//  }
// },
// {
//  path: 'editor/wizard/grading_policy',
//  component: GradingPolicyComponent,
//  data: {
//    pos: 5,
//    total: 10,
//    prev: '/editor/wizard/workload',
//    next: '/editor/wizard/instructor_policy'
//  }
// },
// {
//  path: 'editor/wizard/instructor_policy',
//  component: InstructorPolicyComponent,
//  data: {
//    pos: 6,
//    total: 10,
//    prev: '/editor/wizard/grading_policy',
//    next: '/editor/wizard/unit_policy'
//  }
// },
// {
//  path: 'editor/wizard/unit_policy',
//  component: UnitPolicyComponent,
//  data: {
//    pos: 7,
//    total: 10,
//    prev: '/editor/wizard/instructor_policy',
//    next: '/editor/wizard/campus_policy'
//  }
// },
// {
//  path: 'editor/wizard/campus_policy',
//  component: CampusPolicyComponent,
//  data: {
//    pos: 8,
//    total: 10,
//    prev: '/editor/wizard/unit_policy',
//    next: '/editor/wizard/support'
//  }
// },
// {
//  path: 'editor/wizard/support',
//  component: SupportComponent,
//  data: {
//    pos: 9,
//    total: 10,
//    prev: '/editor/wizard/campus_policy',
//    next: '/editor/wizard/review'
//  }
//
// },
// {
//  path: 'editor/wizard/review',
//  component: ReviewComponent,
//  data: {
//    pos: 10,
//    total: 10,
//    prev: '/editor/wizard/support',
//    next: '/editor/wizard/complete'
//  }
// },
