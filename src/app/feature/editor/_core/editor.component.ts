import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../../_core/BaseComponent';

@Component({
  selector   : 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls  : ['./editor.component.css'],
})
export class EditorComponent extends BaseComponent {
  public title = 'Editor';

  constructor(injector: Injector) {
    super(injector);
  }

  wizard() {
    this.router.navigate(['/editor/wizard/essential'])
      .catch(e => console.log(e));
  }

  manual() {
    this.router.navigate(['/editor/manual'])
      .catch(e => console.log(e));
  }

}
