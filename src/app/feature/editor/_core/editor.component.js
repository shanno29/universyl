"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../../_core/BaseComponent");
var EditorComponent = (function (_super) {
    __extends(EditorComponent, _super);
    function EditorComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.title = 'Editor';
        return _this;
    }

    EditorComponent.prototype.wizard = function () {
        this.router.navigate(['/editor/wizard/essential'])["catch"](function (e) {
            return console.log(e);
        });
    };
    EditorComponent.prototype.manual = function () {
        this.router.navigate(['/editor/manual'])["catch"](function (e) {
            return console.log(e);
        });
    };
    EditorComponent = __decorate([
        core_1.Component({
            selector: 'app-editor',
            templateUrl: './editor.component.html',
            styleUrls: ['./editor.component.css']
        })
    ], EditorComponent);
    return EditorComponent;
}(BaseComponent_1.BaseComponent));
exports.EditorComponent = EditorComponent;
