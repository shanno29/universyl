import {Component, Injector} from '@angular/core';
import {Essential} from '../../../../../domain/syllabus/Essential';
import {BaseComponent} from '../../../../../_core/BaseComponent';
import {Util} from '../../../../../_core/util/Util';

@Component({
  selector   : 'app-essential',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class EssentialComponent extends BaseComponent {
  public title = 'Essential';
  private items = [];

  constructor(public essential: Essential, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.essential);
    console.log(this.items);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.essential[this.items[x].id] = this.items[x].text;
    console.log('[essential]:' + JSON.stringify(this.essential, null, 2));

    this.busManager.publish(this.essential);
    super.ngOnDestroy();
  }

}
