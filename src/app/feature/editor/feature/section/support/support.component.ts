import {Util} from '../../../../../_core/util/Util';
import {Component, Injector} from '@angular/core';
import {Support} from '../../../../../domain/syllabus/Support';
import {BaseComponent} from '../../../../../_core/BaseComponent';

@Component({
  selector   : 'app-support',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class SupportComponent extends BaseComponent {
  public title = 'Support';
  private items = [];

  constructor(public support: Support, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.support);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.support[this.items[x].id] = this.items[x].text;
    console.log('[support]:' + JSON.stringify(this.support, null, 2));

    this.busManager.publish(this.support);
    super.ngOnDestroy();
  }

}
