import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {SupportComponent} from './support.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [SupportComponent],
  exports     : [SupportComponent]
})
export class SupportModule {
}
