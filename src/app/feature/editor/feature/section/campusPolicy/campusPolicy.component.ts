import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../../../../_core/BaseComponent';
import {CampusPolicy} from '../../../../../domain/syllabus/CampusPolicy';
import {Util} from '../../../../../_core/util/Util';

@Component({
  selector   : 'app-campus-policy',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class CampusPolicyComponent extends BaseComponent {
  public title = 'Campus Policy';
  private items = [];

  constructor(public campusPolicy: CampusPolicy, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.campusPolicy);
    console.log(this.items);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.campusPolicy[this.items[x].id] = this.items[x].text;
    console.log('[campusPolicy]:' + JSON.stringify(this.campusPolicy, null, 2));

    this.busManager.publish(this.campusPolicy);
    super.ngOnDestroy();
  }

}
