import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {CampusPolicyComponent} from './campusPolicy.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [CampusPolicyComponent],
  exports     : [CampusPolicyComponent]
})
export class CampusPolicyModule {
}
