import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {InstructorPolicyComponent} from './instructorPolicy.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [InstructorPolicyComponent],
  exports     : [InstructorPolicyComponent]
})
export class InstructorPolicyModule {
}
