import {BaseComponent} from '../../../../../_core/BaseComponent';
import {Component, Injector} from '@angular/core';
import {InstructorPolicy} from '../../../../../domain/syllabus/InstructorPolicy';
import {Util} from '../../../../../_core/util/Util';

@Component({
  selector   : 'app-instructor-policy',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class InstructorPolicyComponent extends BaseComponent {
  public title = 'Instructor Policy';
  private items = [];

  constructor(public instructorPolicy: InstructorPolicy, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.instructorPolicy);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.instructorPolicy[this.items[x].id] = this.items[x].text;
    console.log('[instructorPolicy]:' + JSON.stringify(this.instructorPolicy, null, 2));

    this.busManager.publish(this.instructorPolicy);
    super.ngOnDestroy();
  }

}
