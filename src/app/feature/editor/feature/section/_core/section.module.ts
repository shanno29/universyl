import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {WorkLoadModule} from '../workload/workload.module.';
import {UnitPolicyModule} from '../unitPolicy/unitPolicy.module';
import {MaterialsModule} from '../materials/materials.module';
import {GradingPolicyModule} from '../gradingPolicy/gradingPolicy.module';
import {EssentialModule} from '../essential/essential.module';
import {CourseModule} from '../course/course.module';
import {CampusPolicyModule} from '../campusPolicy/campusPolicy.module';
import {InstructorPolicyModule} from '../instructorPolicy/instructorPolicy.module';
import {SupportModule} from '../support/support.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    CampusPolicyModule,
    CourseModule,
    EssentialModule,
    GradingPolicyModule,
    InstructorPolicyModule,
    MaterialsModule,
    SupportModule,
    UnitPolicyModule,
    WorkLoadModule,
  ],
  declarations: [],
  exports     : [
    CampusPolicyModule,
    CourseModule,
    EssentialModule,
    GradingPolicyModule,
    InstructorPolicyModule,
    MaterialsModule,
    SupportModule,
    UnitPolicyModule,
    WorkLoadModule,
  ]
})
export class SectionModule {
}
