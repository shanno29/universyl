"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var BaseComponent_1 = require("../../../../../_core/BaseComponent");
var core_1 = require("@angular/core");
var Util_1 = require("../../../../../_core/util/Util");
var UnitPolicyComponent = (function (_super) {
    __extends(UnitPolicyComponent, _super);
    function UnitPolicyComponent(unitPolicy, injector) {
        var _this = _super.call(this, injector) || this;
        _this.unitPolicy = unitPolicy;
        _this.title = 'Unit Policy';
        _this.items = [];
        return _this;
    }

    UnitPolicyComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
        this.items = Util_1.Util.getFields(this.unitPolicy);
    };
    UnitPolicyComponent.prototype.ngOnDestroy = function () {
        for (var x = 0; x < this.items.length; x++)
            this.unitPolicy[this.items[x].id] = this.items[x].text;
        console.log('[unitPolicy]:' + JSON.stringify(this.unitPolicy, null, 2));
        this.busManager.publish(this.unitPolicy);
        _super.prototype.ngOnDestroy.call(this);
    };
    UnitPolicyComponent = __decorate([
        core_1.Component({
            selector: 'app-unit-policy',
            templateUrl: '../_core/section.html',
            styleUrls: ['../_core/section.css']
        })
    ], UnitPolicyComponent);
    return UnitPolicyComponent;
}(BaseComponent_1.BaseComponent));
exports.UnitPolicyComponent = UnitPolicyComponent;
