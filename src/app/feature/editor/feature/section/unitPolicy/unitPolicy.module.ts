import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {UnitPolicyComponent} from './unitPolicy.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [UnitPolicyComponent],
  exports     : [UnitPolicyComponent]
})
export class UnitPolicyModule {
}
