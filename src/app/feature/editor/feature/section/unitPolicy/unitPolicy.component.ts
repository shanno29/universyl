import {UnitPolicy} from '../../../../../domain/syllabus/UnitPolicy';
import {BaseComponent} from '../../../../../_core/BaseComponent';
import {Component, Injector} from '@angular/core';
import {Util} from '../../../../../_core/util/Util';
@Component({
  selector   : 'app-unit-policy',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class UnitPolicyComponent extends BaseComponent {
  public title = 'Unit Policy';
  private items = [];

  constructor(public unitPolicy: UnitPolicy, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.unitPolicy);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.unitPolicy[this.items[x].id] = this.items[x].text;
    console.log('[unitPolicy]:' + JSON.stringify(this.unitPolicy, null, 2));

    this.busManager.publish(this.unitPolicy);
    super.ngOnDestroy();
  }

}
