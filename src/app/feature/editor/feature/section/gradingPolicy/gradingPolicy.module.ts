import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {GradingPolicyComponent} from './gradingPolicy.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [GradingPolicyComponent],
  exports     : [GradingPolicyComponent]
})
export class GradingPolicyModule {
}
