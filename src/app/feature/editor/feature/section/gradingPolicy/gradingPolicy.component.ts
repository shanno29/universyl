import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../../../../_core/BaseComponent';
import {GradingPolicy} from '../../../../../domain/syllabus/GradingPolicy';
import {Util} from '../../../../../_core/util/Util';

@Component({
  selector   : 'app-grading-policy',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class GradingPolicyComponent extends BaseComponent {
  public title = 'Grading Policy';
  private items = [];

  constructor(public gradingPolicy: GradingPolicy, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.gradingPolicy);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.gradingPolicy[this.items[x].id] = this.items[x].text;
    console.log('[gradingPolicy]:' + JSON.stringify(this.gradingPolicy, null, 2));

    this.busManager.publish(this.gradingPolicy);
    super.ngOnDestroy();
  }

}
