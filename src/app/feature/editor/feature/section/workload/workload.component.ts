import {Util} from '../../../../../_core/util/Util';
import {Component, Injector} from '@angular/core';
import {Workload} from '../../../../../domain/syllabus/Workload';
import {BaseComponent} from '../../../../../_core/BaseComponent';

@Component({
  selector   : 'app-workload',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class WorkloadComponent extends BaseComponent {
  public title = 'Workload';
  private items = [];

  constructor(public workload: Workload, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.workload);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.workload[this.items[x].id] = this.items[x].text;
    console.log('[workload]:' + JSON.stringify(this.workload, null, 2));

    this.busManager.publish(this.workload);
    super.ngOnDestroy();
  }

}
