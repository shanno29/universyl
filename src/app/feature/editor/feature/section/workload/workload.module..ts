import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {WorkloadComponent} from './workload.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [WorkloadComponent],
  exports     : [WorkloadComponent]
})
export class WorkLoadModule {
}
