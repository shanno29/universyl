"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../../../../_core/BaseComponent");
var Util_1 = require("../../../../../_core/util/Util");
var MaterialsComponent = (function (_super) {
    __extends(MaterialsComponent, _super);
    function MaterialsComponent(materials, injector) {
        var _this = _super.call(this, injector) || this;
        _this.materials = materials;
        _this.title = 'Materials';
        _this.items = [];
        return _this;
    }

    MaterialsComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
        this.items = Util_1.Util.getFields(this.materials);
        console.log(this.items);
    };
    MaterialsComponent.prototype.ngOnDestroy = function () {
        for (var x = 0; x < this.items.length; x++)
            this.materials[this.items[x].id] = this.items[x].text;
        console.log('[materials]:' + JSON.stringify(this.materials, null, 2));
        this.busManager.publish(this.materials);
        _super.prototype.ngOnDestroy.call(this);
    };
    MaterialsComponent = __decorate([
        core_1.Component({
            selector: 'app-materials',
            templateUrl: '../_core/section.html',
            styleUrls: ['../_core/section.css']
        })
    ], MaterialsComponent);
    return MaterialsComponent;
}(BaseComponent_1.BaseComponent));
exports.MaterialsComponent = MaterialsComponent;
