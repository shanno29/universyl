import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../../../../_core/BaseComponent';
import {Util} from '../../../../../_core/util/Util';
import {Materials} from '../../../../../domain/syllabus/Materials';

@Component({
  selector   : 'app-materials',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class MaterialsComponent extends BaseComponent {
  public title = 'Materials';
  private items = [];

  constructor(public materials: Materials, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.materials);
    console.log(this.items);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.materials[this.items[x].id] = this.items[x].text;
    console.log('[materials]:' + JSON.stringify(this.materials, null, 2));

    this.busManager.publish(this.materials);
    super.ngOnDestroy();
  }

}
