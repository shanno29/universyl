import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {MaterialsComponent} from './materials.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
  ],
  declarations: [MaterialsComponent],
  exports     : [MaterialsComponent]
})
export class MaterialsModule {
}
