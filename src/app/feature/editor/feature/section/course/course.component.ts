import {Util} from '../../../../../_core/util/Util';
import {Component, Injector} from '@angular/core';
import {Course} from '../../../../../domain/syllabus/Course';
import {BaseComponent} from '../../../../../_core/BaseComponent';

@Component({
  selector   : 'app-course',
  templateUrl: '../_core/section.html',
  styleUrls  : ['../_core/section.css']
})
export class CourseComponent extends BaseComponent {
  public title = 'Course';
  private items = [];

  constructor(public course: Course, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.items = Util.getFields(this.course);
  }

  ngOnDestroy() {
    for (let x = 0; x < this.items.length; x++) this.course[this.items[x].id] = this.items[x].text;
    console.log('[course]:' + JSON.stringify(this.course, null, 2));

    this.busManager.publish(this.course);
    super.ngOnDestroy();
  }

}
