"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var Util_1 = require("../../../../../_core/util/Util");
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../../../../_core/BaseComponent");
var CourseComponent = (function (_super) {
    __extends(CourseComponent, _super);
    function CourseComponent(course, injector) {
        var _this = _super.call(this, injector) || this;
        _this.course = course;
        _this.title = 'Course';
        _this.items = [];
        return _this;
    }

    CourseComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
        this.items = Util_1.Util.getFields(this.course);
    };
    CourseComponent.prototype.ngOnDestroy = function () {
        for (var x = 0; x < this.items.length; x++)
            this.course[this.items[x].id] = this.items[x].text;
        console.log('[course]:' + JSON.stringify(this.course, null, 2));
        this.busManager.publish(this.course);
        _super.prototype.ngOnDestroy.call(this);
    };
    CourseComponent = __decorate([
        core_1.Component({
            selector: 'app-course',
            templateUrl: '../_core/section.html',
            styleUrls: ['../_core/section.css']
        })
    ], CourseComponent);
    return CourseComponent;
}(BaseComponent_1.BaseComponent));
exports.CourseComponent = CourseComponent;
