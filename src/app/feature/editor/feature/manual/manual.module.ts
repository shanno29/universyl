import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {ManualComponent} from './manual.component';
import {SectionModule} from '../section/_core/section.module';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    SectionModule,
  ],
  declarations: [ManualComponent],
  exports     : [ManualComponent]
})
export class ManualModule {
}
