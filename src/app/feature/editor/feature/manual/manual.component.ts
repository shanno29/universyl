import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../../../_core/BaseComponent';

@Component({
  selector   : 'app-manual',
  templateUrl: './manual.component.html',
  styleUrls  : ['./manual.component.css'],
})
export class ManualComponent extends BaseComponent {
  public title = 'Editor - Manual';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

  }

}
