import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {WizardComponent} from './wizard.component';
import {SectionModule} from '../section/_core/section.module';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    SectionModule,
  ],
  declarations: [WizardComponent],
  exports     : [WizardComponent]
})
export class WizardModule {
}
