import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../../../_core/BaseComponent';

@Component({
  selector   : 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls  : ['./wizard.component.css'],
})
export class WizardComponent extends BaseComponent {
  public title = 'Editor - Wizard';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

  }

}
