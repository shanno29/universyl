"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../../../_core/BaseComponent");
var Syllabus_1 = require("../../../../domain/syllabus/Syllabus");
var ReviewComponent = (function (_super) {
    __extends(ReviewComponent, _super);
    function ReviewComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.title = 'Editor - Review';
        return _this;
    }

    ReviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        _super.prototype.ngOnInit.call(this);
        this.sub.add(this.busManager.of(Syllabus_1.Syllabus).subscribe(function (x) {
            return _this.output = JSON.stringify(x, null, 2);
        }));
    };
    ReviewComponent.prototype.prev = function () {
        this.router.navigate(['/editor/wizard/support'])["catch"](function (e) {
            return console.log(e);
        });
    };
    ReviewComponent.prototype.next = function () {
        this.router.navigate(['/editor/editor'])["catch"](function (e) {
            return console.log(e);
        });
    };
    ReviewComponent = __decorate([
        core_1.Component({
            selector: 'app-editor',
            templateUrl: './review.component.html',
            styleUrls: ['./review.component.css']
        })
    ], ReviewComponent);
    return ReviewComponent;
}(BaseComponent_1.BaseComponent));
exports.ReviewComponent = ReviewComponent;
