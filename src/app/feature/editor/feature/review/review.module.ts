import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {ReviewComponent} from './review.component';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
  ],
  declarations: [ReviewComponent],
  exports     : [ReviewComponent]
})
export class ReviewModule {
}
