import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../../../_core/BaseComponent';
import {Syllabus} from '../../../../domain/syllabus/Syllabus';

@Component({
  selector   : 'app-editor',
  templateUrl: './review.component.html',
  styleUrls  : ['./review.component.css']
})
export class ReviewComponent extends BaseComponent {
  public title = 'Editor - Review';
  private output: String;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.sub.add(this.busManager.of(Syllabus).subscribe(x => this.output = JSON.stringify(x, null, 2)));
  }

  prev() {
    this.router.navigate(['/editor/wizard/support'])
      .catch(e => console.log(e));
  }

  next() {
    this.router.navigate(['/editor/editor'])
      .catch(e => console.log(e));
  }

}

