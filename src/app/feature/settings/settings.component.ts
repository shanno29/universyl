import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../_core/BaseComponent';
import {User} from '../../domain/user/User';
import {MemberType} from '../../domain/user/MemberType';

@Component({
  selector   : 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls  : ['./settings.component.css']
})
export class SettingsComponent extends BaseComponent {
  public title = 'Settings';
  public MemberType = MemberType;

  constructor(public user: User, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    // Initial User Data Call
    this.user = this.storageManager.getObject('user');

    // Listen For User Updates
    this.sub.add(this.storageManager
      .getObjectRx('user')
      .subscribe(res => this.user = this.storageManager.getObject(res.key))
    );

  }

  public onTypeChange(pos: number) {
    const user = this.storageManager.getObject('user');
    if (pos === 0) user.type = MemberType.Student;
    if (pos === 1) user.type = MemberType.Faculty;
    if (pos === 2) user.type = MemberType.Admin;
    this.storageManager.setObject('user', user);
  }

}
