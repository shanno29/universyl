"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../_core/BaseComponent");
var MemberType_1 = require("../../domain/user/MemberType");
var SettingsComponent = (function (_super) {
    __extends(SettingsComponent, _super);
    function SettingsComponent(user, injector) {
        var _this = _super.call(this, injector) || this;
        _this.user = user;
        _this.title = 'Settings';
        _this.MemberType = MemberType_1.MemberType;
        return _this;
    }

    SettingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        _super.prototype.ngOnInit.call(this);
        // Initial User Data Call
        this.user = this.storageManager.getObject('user');
        // Listen For User Updates
        this.sub.add(this.storageManager
            .getObjectRx('user')
            .subscribe(function (res) {
                return _this.user = _this.storageManager.getObject(res.key);
            }));
    };
    SettingsComponent.prototype.onTypeChange = function (pos) {
        var user = this.storageManager.getObject('user');
        if (pos === 0)
            user.type = MemberType_1.MemberType.Student;
        if (pos === 1)
            user.type = MemberType_1.MemberType.Faculty;
        if (pos === 2)
            user.type = MemberType_1.MemberType.Admin;
        this.storageManager.setObject('user', user);
    };
    SettingsComponent = __decorate([
        core_1.Component({
            selector: 'app-settings',
            templateUrl: './settings.component.html',
            styleUrls: ['./settings.component.css']
        })
    ], SettingsComponent);
    return SettingsComponent;
}(BaseComponent_1.BaseComponent));
exports.SettingsComponent = SettingsComponent;
