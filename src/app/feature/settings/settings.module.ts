import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettingsComponent} from './settings.component';
import {MaterialModule} from '@angular/material';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{path: 'settings', component: SettingsComponent}])
  ],
  declarations: [SettingsComponent],
  exports     : [SettingsComponent]
})
export class SettingsModule {
}
