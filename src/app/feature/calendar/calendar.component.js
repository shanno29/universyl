"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../_core/BaseComponent");
var CircleUtil_1 = require("../../_core/util/CircleUtil");
var CalendarComponent = (function (_super) {
    __extends(CalendarComponent, _super);
    function CalendarComponent(injector, deadline) {
        var _this = _super.call(this, injector) || this;
        _this.deadline = deadline;
        _this.title = 'Calendar';
        _this.circles = [
            {
                x: 1000.0, y: 1000.0, r: 1000.0, color: CircleUtil_1.CircleUtil.randColor, sectors: [
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
            ]
            },
        ];
        _this.pathRotate = function (circle, pos) {
            return "rotate(" + CircleUtil_1.CircleUtil.getAngle(circle, pos) + "," + circle.x + "," + circle.y + ")";
        };
        return _this;
    }

    CalendarComponent.prototype.ngAfterViewInit = function () {
        // for (let x = 0; x < 361; x++) {
        //   TweenLite.to("#background", 250, {rotation: x, svgOrigin: "1000 1000", force3D: false});
        //   TweenLite.to("#piece", 250, {rotation: x, svgOrigin: "1000.0px 1000.0px", scale: -10.0, force3D: false});
        // }
    };
    CalendarComponent.prototype.secClick = function (sector) {
        // this.infoData = sector.text;
    };
    CalendarComponent.prototype.pathData = function (circle) {
        var angle = (360.0 / circle.sectors.length);
        return ''
            + ("M" + circle.x + "," + circle.y + " ")
            + ("l" + circle.r + ",0 ")
            + ("A" + circle.r + "," + circle.r + " 0 " + (angle > 180 ? 1 : 0) + ",0 ")
            + (CircleUtil_1.CircleUtil.getRX(circle.x, circle.r, angle) + ",")
            + (CircleUtil_1.CircleUtil.getRY(circle.y, circle.r, angle) + " ")
            + "z";
    };
    CalendarComponent = __decorate([
        core_1.Component({
            selector: 'app-calendar',
            templateUrl: './calendar.component.html',
            styleUrls: ['./calendar.component.css'],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        })
    ], CalendarComponent);
    return CalendarComponent;
}(BaseComponent_1.BaseComponent));
exports.CalendarComponent = CalendarComponent;
