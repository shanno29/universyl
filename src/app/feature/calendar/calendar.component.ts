import {AfterViewInit, ChangeDetectionStrategy, Component, Injector} from '@angular/core';
import {BaseComponent} from '../../_core/BaseComponent';
import {Deadline} from '../../domain/calendar/Deadline';
import {CircleUtil} from '../../_core/util/CircleUtil';

@Component({
  selector       : 'app-calendar',
  templateUrl    : './calendar.component.html',
  styleUrls      : ['./calendar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarComponent extends BaseComponent implements AfterViewInit {
  public title = 'Calendar';

  circles = [
    {
      x: 1000.0, y: 1000.0, r: 1000.0, color: CircleUtil.randColor, sectors: [
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
    ]
    },
  ];


  constructor(injector: Injector, public deadline: Deadline) {
    super(injector);
  }

  ngAfterViewInit() {

    // for (let x = 0; x < 361; x++) {
    //   TweenLite.to("#background", 250, {rotation: x, svgOrigin: "1000 1000", force3D: false});
    //   TweenLite.to("#piece", 250, {rotation: x, svgOrigin: "1000.0px 1000.0px", scale: -10.0, force3D: false});
    // }

  }

  secClick(sector) {
    // this.infoData = sector.text;
  }

  pathRotate = (circle, pos) => `rotate(${CircleUtil.getAngle(circle, pos)},${circle.x},${circle.y})`;

  pathData(circle) {
    const angle = (360.0 / circle.sectors.length);
    return ''
      + `M${circle.x},${circle.y} `
      + `l${circle.r},0 `
      + `A${circle.r},${circle.r} 0 ${angle > 180 ? 1 : 0},0 `
      + `${CircleUtil.getRX(circle.x, circle.r, angle)},`
      + `${CircleUtil.getRY(circle.y, circle.r, angle)} `
      + `z`;
  }

}
