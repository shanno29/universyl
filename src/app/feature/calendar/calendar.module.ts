import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {CalendarComponent} from './calendar.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{path: 'calendar', component: CalendarComponent}])
  ],
  declarations: [CalendarComponent],
  exports     : [CalendarComponent]
})
export class CalendarModule {
}
