"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../_core/BaseComponent");
var Department_1 = require("../../domain/department/Department");
var StatsComponent = (function (_super) {
    __extends(StatsComponent, _super);
    function StatsComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.title = 'Stats';
        _this.query = '';
        return _this;
        // this.controller = new StatsController(this.http);
    }

    StatsComponent.prototype.ngOnInit = function () {
        var _this = this;
        _super.prototype.ngOnInit.call(this);
        this.sub.add(this.dataManager.getAllDepartments()
            .subscribe(function (res) {
                _this.masterList = [];
                for (var i = 0; i < res.length; i++) {
                    _this.masterList.push(new Department_1.Department(res[i]));
                }
                _this.searchList = _this.masterList;
                console.log(_this.masterList);
            }, function (err) {
                return console.log(err);
            }));
        // this.sub.add(
        //   this.dataManager
        //     .getAllDepartments()
        //     .subscribe(
        //       res => console.log(res),
        //       err => console.log(err)
        //     )
        // );
        // this.sub.add(this.controller.getData().subscribe(res => {
        //   console.log(res);
        //   this.masterList = res;
        //   this.searchList = res;
        // }));
    };
    StatsComponent.prototype.textChanged = function () {
        var _this = this;
        console.log(this.query);
        this.searchList = [];
        this.masterList.forEach(function (department) {
            return _this.searchList.push(department.filterDept(_this.query));
        });
        this.searchList = this.searchList.sort(function (a, b) {
            return b.count() - a.count();
        });
    };
    StatsComponent = __decorate([
        core_1.Component({
            selector: 'app-stats',
            templateUrl: './stats.component.html',
            styleUrls: ['./stats.component.css']
        })
    ], StatsComponent);
    return StatsComponent;
}(BaseComponent_1.BaseComponent));
exports.StatsComponent = StatsComponent;
