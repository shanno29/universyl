"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var stats_component_1 = require("./stats.component");
var material_1 = require("@angular/material");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var StatsModule = (function () {
    function StatsModule() {
    }

    StatsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                forms_1.FormsModule,
                router_1.RouterModule.forChild([{path: 'stats', component: stats_component_1.StatsComponent}])
            ],
            declarations: [stats_component_1.StatsComponent],
            exports: [stats_component_1.StatsComponent]
        })
    ], StatsModule);
    return StatsModule;
}());
exports.StatsModule = StatsModule;
