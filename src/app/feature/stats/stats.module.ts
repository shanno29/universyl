import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StatsComponent} from './stats.component';
import {MaterialModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {Keys, Values} from '../../_core/map.values.pipe';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule.forChild([{path: 'stats', component: StatsComponent}])
  ],
  declarations: [StatsComponent, Values, Keys],
  exports     : [StatsComponent]
})
export class StatsModule {
}
