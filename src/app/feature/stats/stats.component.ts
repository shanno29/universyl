import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../_core/BaseComponent';
import {Department} from '../../domain/department/Department';
import {Constant} from '../../_core/Constant';
import {DataManager} from '../../manager/data/DataManager';
import {Program} from '../../domain/program/Program';

@Component({
  selector   : 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls  : ['./stats.component.css']
})
export class StatsComponent extends BaseComponent {
  public title = 'Stats';
  public dataManager;

  public filteredDepartments: Department[];
  public departments: Department[];

  public filteredPrograms: Program[];
  public programs: Program[];

  public type = 0;
  public query: String = '';

  constructor(injector: Injector) {
    super(injector);
    this.data = this.http.get(Constant.API)
      .map(v => v.json()[Constant.DATA])
      .subscribe(res => {
        this.dataManager = new DataManager(res);

        const departments = this.dataManager.getAllDepartments();
        departments.forEach(v => v.programs = this.dataManager.getProgramsByIds(v.programs) as Program[]);
        this.filteredDepartments = departments;
        this.departments = departments;
        console.log(this.departments);

        const programs = this.dataManager.getAllPrograms();
        this.filteredPrograms = programs;
        this.programs = programs;
        console.log(this.programs);
      });
  }

  ngOnInit() {
    super.ngOnInit();

  }

  public textChanged() {
    console.log(this.query);

    if (this.type === 0) {
      this.filteredDepartments = [];
      this.departments.forEach(v => { if (v.filterDept(this.query)) this.filteredDepartments.push(v); } );
      this.filteredDepartments = this.filteredDepartments.sort((a, b) => b.count() - a.count());
    }

    else if (this.type === 1) {
      this.filteredPrograms = [];
      this.programs.forEach(v => { if (v.filterProg(this.query)) this.filteredPrograms.push(v); } );
      this.filteredPrograms = this.filteredPrograms.sort((a, b) => b.count() - a.count());
    }

    // this.searchList = [];
    // this.masterList.forEach(department => this.searchList.push(department.filterDept(this.query)));
    // this.searchList = this.searchList.sort((a, b) => b.count() - a.count());
  }

  public typeChanged(pos: number){
    this.type = pos;
    this.query = '';
    console.log(pos);
  }

  public getSection(value: string, dept: Department): Program[] {
    return dept.programs.filter(v => v.type === value);

  }

}

