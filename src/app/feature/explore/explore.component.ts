import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../_core/BaseComponent';

@Component({
  selector   : 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls  : ['./explore.component.css'],
})
export class ExploreComponent extends BaseComponent {
  public title = 'Explore';
  temp = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.';
  buttons = [
    {id: 'button1', text: 'Math', expanded: false, color: '#F44336'},
    {id: 'button2', text: 'Science', expanded: false, color: '#3F51B5'},
    {id: 'button3', text: 'Arts', expanded: false, color: '#388E3C'},
    {id: 'button4', text: 'Engineering', expanded: false, color: '#673AB7'},
    {id: 'button5', text: 'Language', expanded: false, color: '#EC407A'}
  ];
  current = '#FFF';
  cards = [];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

  }

  buttonClicked(i) {
    // clear cards
    this.cards = [];

    // get button
    const b = this.buttons[i];

    // invert button
    b.expanded = !b.expanded;

    // reset other buttons
    for (let x = 0; x < this.buttons.length; x++) {
      if (x !== i) {
        this.buttons[x].expanded = false;
      }
    }

    // set color
    this.current = b.expanded ? b.color : '#FFF';

    // add card
    if (b.expanded) {
      this.addCard(i);
    }

    // clear cards
    else {
      this.clearAll();
    }


  }

  addCard(i) {
    this.cards.push({id: 'card' + i, text: this.temp, expanded: false});

  }

  cardSelected(i) {
    // get card
    const c = this.cards[i];

    // invert card
    c.expanded = !c.expanded;

    // add card
    if (c.expanded) {
      this.addCard(i + 1);
    }

    // clear cards
    else {

      this.cards[i].expanded = false;
      this.cards.splice(i + 1, this.cards.length - i);

    }
  }

  clearAll() {
    this.cards = [];
    for (let x = 0; x < this.buttons.length; x++) {
      this.buttons[x].expanded = false;
    }
  }

  changeColor(hex, lum) {

    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    let rgb = '#', c, i;
    for (i = 0; i < 3; i++) {
      c = parseInt(hex.substr(i * 2, 2), 16);
      c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
      rgb += ('00' + c).substr(c.length);
    }

    return rgb;

  }

}
