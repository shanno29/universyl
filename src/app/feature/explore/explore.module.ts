import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {ExploreComponent} from './explore.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{path: 'explore', component: ExploreComponent}])
  ],
  declarations: [ExploreComponent],
  exports     : [ExploreComponent]
})
export class ExploreModule {
}
