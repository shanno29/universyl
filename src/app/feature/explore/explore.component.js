"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../_core/BaseComponent");
var ExploreComponent = (function (_super) {
    __extends(ExploreComponent, _super);
    function ExploreComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.title = 'Explore';
        _this.temp = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.';
        _this.buttons = [
            {id: 'button1', text: 'Math', expanded: false, color: '#F44336'},
            {id: 'button2', text: 'Science', expanded: false, color: '#3F51B5'},
            {id: 'button3', text: 'Arts', expanded: false, color: '#388E3C'},
            {id: 'button4', text: 'Engineering', expanded: false, color: '#673AB7'},
            {id: 'button5', text: 'Language', expanded: false, color: '#EC407A'}
        ];
        _this.current = '#FFF';
        _this.cards = [];
        return _this;
    }

    ExploreComponent.prototype.ngOnInit = function () {
        _super.prototype.ngOnInit.call(this);
    };
    ExploreComponent.prototype.buttonClicked = function (i) {
        // clear cards
        this.cards = [];
        // get button
        var b = this.buttons[i];
        // invert button
        b.expanded = !b.expanded;
        // reset other buttons
        for (var x = 0; x < this.buttons.length; x++) {
            if (x !== i) {
                this.buttons[x].expanded = false;
            }
        }
        // set color
        this.current = b.expanded ? b.color : '#FFF';
        // add card
        if (b.expanded) {
            this.addCard(i);
        }
        else {
            this.clearAll();
        }
    };
    ExploreComponent.prototype.addCard = function (i) {
        this.cards.push({id: 'card' + i, text: this.temp, expanded: false});
    };
    ExploreComponent.prototype.cardSelected = function (i) {
        // get card
        var c = this.cards[i];
        // invert card
        c.expanded = !c.expanded;
        // add card
        if (c.expanded) {
            this.addCard(i + 1);
        }
        else {
            this.cards[i].expanded = false;
            this.cards.splice(i + 1, this.cards.length - i);
        }
    };
    ExploreComponent.prototype.clearAll = function () {
        this.cards = [];
        for (var x = 0; x < this.buttons.length; x++) {
            this.buttons[x].expanded = false;
        }
    };
    ExploreComponent.prototype.changeColor = function (hex, lum) {
        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        lum = lum || 0;
        // convert to decimal and change luminosity
        var rgb = '#', c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i * 2, 2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ('00' + c).substr(c.length);
        }
        return rgb;
    };
    ExploreComponent = __decorate([
        core_1.Component({
            selector: 'app-explore',
            templateUrl: './explore.component.html',
            styleUrls: ['./explore.component.css']
        })
    ], ExploreComponent);
    return ExploreComponent;
}(BaseComponent_1.BaseComponent));
exports.ExploreComponent = ExploreComponent;
