import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../_core/BaseComponent';

@Component({
  selector   : 'app-about',
  templateUrl: './about.component.html',
  styleUrls  : ['./about.component.css']
})
export class AboutComponent extends BaseComponent {
  public title = 'About';

  constructor(injector: Injector) {
    super(injector);
  }

}
