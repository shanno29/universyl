import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {AboutComponent} from './about.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{path: 'about', component: AboutComponent}])
  ],
  declarations: [AboutComponent],
})
export class AboutModule {
}
