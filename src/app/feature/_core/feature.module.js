"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var material_1 = require("@angular/material");
var editor_module_1 = require("../editor/_core/editor.module");
var degree_module_1 = require("../degree/degree.module");
var calendar_module_1 = require("../calendar/calendar.module");
var help_module_1 = require("../help/help.module");
var explore_module_1 = require("../explore/explore.module");
var home_module_1 = require("../home/home.module");
var settings_module_1 = require("../settings/settings.module");
var stats_module_1 = require("../stats/stats.module");
var about_module_1 = require("../about/about.module");
var FeatureModule = (function () {
    function FeatureModule() {
    }

    FeatureModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                about_module_1.AboutModule,
                calendar_module_1.CalendarModule,
                degree_module_1.DegreeModule,
                editor_module_1.EditorModule,
                explore_module_1.ExploreModule,
                help_module_1.HelpModule,
                home_module_1.HomeModule,
                settings_module_1.SettingsModule,
                stats_module_1.StatsModule,
            ],
            declarations: [],
            exports: [
                about_module_1.AboutModule,
                calendar_module_1.CalendarModule,
                degree_module_1.DegreeModule,
                editor_module_1.EditorModule,
                explore_module_1.ExploreModule,
                help_module_1.HelpModule,
                home_module_1.HomeModule,
                settings_module_1.SettingsModule,
                stats_module_1.StatsModule,
            ]
        })
    ], FeatureModule);
    return FeatureModule;
}());
exports.FeatureModule = FeatureModule;
