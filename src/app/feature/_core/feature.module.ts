import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {EditorModule} from '../editor/_core/editor.module';
import {DegreeModule} from '../degree/degree.module';
import {CalendarModule} from '../calendar/calendar.module';
import {HelpModule} from '../help/help.module';
import {ExploreModule} from '../explore/explore.module';
import {HomeModule} from '../home/home.module';
import {SettingsModule} from '../settings/settings.module';
import {StatsModule} from '../stats/stats.module';
import {AboutModule} from '../about/about.module';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    AboutModule,
    CalendarModule,
    DegreeModule,
    EditorModule,
    ExploreModule,
    HelpModule,
    HomeModule,
    SettingsModule,
    StatsModule,
  ],
  declarations: [],
  exports     : [
    AboutModule,
    CalendarModule,
    DegreeModule,
    EditorModule,
    ExploreModule,
    HelpModule,
    HomeModule,
    SettingsModule,
    StatsModule,
  ]
})
export class FeatureModule {
}
