"use strict";
var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({__proto__: []} instanceof Array && function (d, b) {
                d.__proto__ = b;
            }) ||
            function (d, b) {
                for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
            };
        return function (d, b) {
            extendStatics(d, b);
            function __() {
                this.constructor = d;
            }

            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
exports.__esModule = true;
var core_1 = require("@angular/core");
var BaseComponent_1 = require("../../_core/BaseComponent");
var CircleUtil_1 = require("../../_core/util/CircleUtil");
var DegreeComponent = (function (_super) {
    __extends(DegreeComponent, _super);
    function DegreeComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.title = 'Degree';
        // depth = 1;
        // radius = 400;
        _this.infoData = '';
        _this.circles = [
            {
                x: 1000, y: 1000, r: 1000, color: CircleUtil_1.CircleUtil.randColor, sectors: [
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
            ]
            },
            {
                x: 1000, y: 1000, r: 800, color: CircleUtil_1.CircleUtil.randColor, sectors: [
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
            ]
            },
            {
                x: 1000, y: 1000, r: 600, color: CircleUtil_1.CircleUtil.randColor, sectors: [
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
            ]
            },
            {
                x: 1000, y: 1000, r: 400, color: CircleUtil_1.CircleUtil.randColor, sectors: [
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
            ]
            },
            {
                x: 1000, y: 1000, r: 200, color: CircleUtil_1.CircleUtil.randColor, sectors: [
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
                {text: CircleUtil_1.CircleUtil.randColor(), color: CircleUtil_1.CircleUtil.randColor()},
            ]
            },
        ];
        _this.getAngle = function (circle, pos) {
            return 360.0 / circle.sectors.length * pos;
        };
        _this.pathRotate = function (circle, pos) {
            return "rotate(" + _this.getAngle(circle, pos) + "," + circle.x + "," + circle.y + ")";
        };
        return _this;
    }

    DegreeComponent.prototype.pathData = function (circle) {
        var angle = (360.0 / circle.sectors.length);
        return ''
            + ("M" + circle.x + "," + circle.y + " ")
            + ("l" + circle.r + ",0 ")
            + ("A" + circle.r + "," + circle.r + " 0 " + (angle > 180 ? 1 : 0) + ",0 ")
            + (CircleUtil_1.CircleUtil.getRX(circle.x, circle.r, angle) + ",")
            + (CircleUtil_1.CircleUtil.getRY(circle.y, circle.r, angle) + " ")
            + "z";
    };
    DegreeComponent.prototype.secClick = function (sector) {
        this.infoData = sector.text;
        // this.depth++;
        // this.radius += this.radius;
        //
        // let items = [];
        // let limit = this.randNumber(1, 8);
        // for(let x=0; x<limit; x++) items.push({text: this.randColor(), color:this.randColor()});
        // this.circles.push({x: 1000, y: 1000, r: this.radius, color:this.randColor, sectors: items});
    };
    DegreeComponent = __decorate([
        core_1.Component({
            selector: 'app-degree',
            templateUrl: './degree.component.html',
            styleUrls: ['./degree.component.css'],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        })
    ], DegreeComponent);
    return DegreeComponent;
}(BaseComponent_1.BaseComponent));
exports.DegreeComponent = DegreeComponent;
