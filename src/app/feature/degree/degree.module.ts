import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {DegreeComponent} from './degree.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{path: 'degree', component: DegreeComponent}])
  ],
  declarations: [DegreeComponent],
  exports     : [DegreeComponent]
})
export class DegreeModule {
}
