import {ChangeDetectionStrategy, Component, Injector} from '@angular/core';
import {BaseComponent} from '../../_core/BaseComponent';
import {CircleUtil} from '../../_core/util/CircleUtil';

@Component({
  selector       : 'app-degree',
  templateUrl    : './degree.component.html',
  styleUrls      : ['./degree.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DegreeComponent extends BaseComponent {
  public title = 'Degree';
  // depth = 1;
  // radius = 400;
  infoData = '';
  circles = [
    {
      x: 1000, y: 1000, r: 1000, color: CircleUtil.randColor, sectors: [
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
    ]
    },
    {
      x: 1000, y: 1000, r: 800, color: CircleUtil.randColor, sectors: [
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
    ]
    },
    {
      x: 1000, y: 1000, r: 600, color: CircleUtil.randColor, sectors: [
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
    ]
    },
    {
      x: 1000, y: 1000, r: 400, color: CircleUtil.randColor, sectors: [
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
    ]
    },
    {
      x: 1000, y: 1000, r: 200, color: CircleUtil.randColor, sectors: [
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
      {text: CircleUtil.randColor(), color: CircleUtil.randColor()},
    ]
    },
  ];

  getAngle = (circle, pos) => 360.0 / circle.sectors.length * pos;
  pathRotate = (circle, pos) => `rotate(${this.getAngle(circle, pos)},${circle.x},${circle.y})`;

  pathData(circle) {
    const angle = (360.0 / circle.sectors.length);
    return ''
      + `M${circle.x},${circle.y} `
      + `l${circle.r},0 `
      + `A${circle.r},${circle.r} 0 ${angle > 180 ? 1 : 0},0 `
      + `${CircleUtil.getRX(circle.x, circle.r, angle)},`
      + `${CircleUtil.getRY(circle.y, circle.r, angle)} `
      + `z`;
  }

  constructor(injector: Injector) {
    super(injector);
  }

  secClick(sector) {
    this.infoData = sector.text;
    // this.depth++;
    // this.radius += this.radius;
    //
    // let items = [];
    // let limit = this.randNumber(1, 8);
    // for(let x=0; x<limit; x++) items.push({text: this.randColor(), color:this.randColor()});
    // this.circles.push({x: 1000, y: 1000, r: this.radius, color:this.randColor, sectors: items});


  }

}
