import {Component, Injector} from '@angular/core';
import {BaseComponent} from '../../_core/BaseComponent';

@Component({
  selector   : 'app-home',
  templateUrl: './home.component.html',
  styleUrls  : ['./home.component.css']
})
export class HomeComponent extends BaseComponent {

  public title = 'Universyl';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();


  }


}


