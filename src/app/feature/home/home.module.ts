import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {HomeComponent} from './home.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([{path: 'home', component: HomeComponent}])
  ],
  declarations: [HomeComponent],
})
export class HomeModule {
}
